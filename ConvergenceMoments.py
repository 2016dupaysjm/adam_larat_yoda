# -*- coding: utf-8 -*-
"""
Created on Mon May 30 15:06:30 2016

@author: larat
"""

import numpy as np
from main import Main
import pylab

NN = np.array([10,20,40,80,160,320])
nMeshes  = np.size(NN)
nMoments = 4
dx      = np.zeros(nMeshes)
ErrL1   = np.zeros((nMeshes,nMoments))
ErrL2   = np.zeros((nMeshes,nMoments))
ErrLInf = np.zeros((nMeshes,nMoments))
for i in np.arange(nMeshes)    :
    print "****** Compute N=",NN[i]," *******************"
    dx[i],ErrL1[i],ErrL2[i],ErrLInf[i]=Main(N=NN[i],doPlot=False)
pL1 = np.zeros(nMoments);pL2=np.zeros(nMoments);pLInf=np.zeros(nMoments)
yL1 = np.zeros(nMoments);yL2=np.zeros(nMoments);yLInf=np.zeros(nMoments)
from scipy import stats
for i in np.arange(nMoments):
    [pL1[i],yL1[i],r,p,caca] = stats.linregress(np.log(dx),np.log(ErrL1[:,i]))
    [pL2[i],yL2[i],r,p,caca] = stats.linregress(np.log(dx),np.log(ErrL2[:,i]))
    [pLInf[i],yLInf[i],r,p,caca] = stats.linregress(np.log(dx),np.log(ErrLInf[:,i]))
pylab.close('all')
markers = ['+','x','*','d','o','^','v','<','>']
nMarkers = np.size(markers)
### L1-Norm
pylab.figure(1) 
pylab.title("Convergence in the L1-Norm")
pylab.xlabel("dx")
pylab.ylabel("L1-Error")
for i in np.arange(nMoments):
    pylab.loglog(dx,ErrL1[:,i],markers[i%nMarkers]+'-',lw=4,ms=12,mew=4,label="m_"+str(i))
pylab.loglog(dx,np.exp(pL1[0]*np.log(dx)+yL1[0]),'k',lw=2,label=format(pL1[0],'+1.2f')+' log(N) '+format(yL1[0],'+1.2f'))
pylab.legend(loc='lower right')
### L2-Norm
pylab.figure(2) 
pylab.title("Convergence in the L2-Norm")
pylab.xlabel("dx")
pylab.ylabel("L2-Error")
for i in np.arange(nMoments):
    pylab.loglog(dx,ErrL2[:,i],markers[i%nMarkers]+'-',lw=4,ms=12,mew=4,label="m_"+str(i))
pylab.loglog(dx,np.exp(pL2[0]*np.log(dx)+yL2[0]),'k',lw=2,label=format(pL2[0],'+1.2f')+' log(N) '+format(yL2[0],'+1.2f'))
pylab.legend(loc='lower right')
### LInf-Norm
pylab.figure(3) 
pylab.title("Convergence in the LInf-Norm")
pylab.xlabel("dx")
pylab.ylabel("LInf-Error")
for i in np.arange(nMoments):
    pylab.loglog(dx,ErrLInf[:,i],markers[i%nMarkers]+'-',lw=4,ms=12,mew=4,label="m_"+str(i))
pylab.loglog(dx,np.exp(pLInf[0]*np.log(dx)+yLInf[0]),'k',lw=2,label=format(pLInf[0],'+1.2f')+' log(N) '+format(yLInf[0],'+1.2f'))
pylab.legend(loc='lower right')