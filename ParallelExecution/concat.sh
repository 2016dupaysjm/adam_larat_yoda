#!/bin/bash
cd Execute
for O in {1,2,3}
do
  for M in {2..8} 
  do 
    mkdir -p Order${O}/Convergence_${M}Moments 
  done
done
for T in {Border,Gamma,Beta}
do 
  for O in {1,2,3}
  do
    for M in {2..8} 
    do 
      for N in {32,64,128,256,512,1024} 
      do 
        echo "***********************************"
        echo "** T = "$T", O = "$O", N = "$N"  **"
        echo "***********************************"
        cat out_momentsAdv-moments${T}_Order${O}_${M}Moments_N${N}.log
      done > Order${O}/Convergence_${M}Moments/out_momentsAdv-moments${T}_Order${O}.log
    done
  done
done
mv Order[1-3] ../.
cd ..
