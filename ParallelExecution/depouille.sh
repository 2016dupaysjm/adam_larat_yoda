#!/bin/bash
for T in {Beta,Gamma,Border}
do 
  for O in {1,2,3}
  do 
    for M in {2..8}
    do 
      rootname="momentsAdv-moments${T}_Order${O}"
      path="Order${O}/Convergence_${M}Moments"
      echo "Treating Order${O}/Convergence_${M}Moments/out_${rootname}.log"
      cat ${path}/out_${rootname}.log | grep -e ^[NL] | grep -v "Norm" | grep -v "Loss" | sed -e 's/inf/i/'| sed -e ':z;N;s/\]*\nL[1|2|i] Error =  \[//;bz' | sed -e 's/Number of Cells: N =  //' -e 's/\]$//' > ${path}/Convergence_${rootname}.dat
      python convergence.py ${path}/Convergence_${rootname}.dat
    done
  done
done

