#!/bin/bash
for T in {Border,Gamma,Beta}
do 
  for O in 1 #{1,2,3}
  do
    for M in {2..8} 
    do 
      for N in {32,64,128,256,512,1024} 
      do 
        echo "Parameters/momentsAdv-moments${T}_Order${O}_${M}Moments_N${N}-parameters.py"
        sed -e "s/NNN/${N}/" -e "s/TTT/${T}/" -e "s/MMM/${M}/" parametersNT${O}.py > Parameters/momentsAdv-moments${T}_Order${O}_${M}Moments_N${N}-parameters.py
      done
    done
  done
done
