#!/bin/bash

if [ $# -ne 1 ]
then
  echo "This script needs one argument, the name of the parameters file"
  echo "Syntax:"
  echo "  If one wants to run a simple test case: runOneTest.sh root-parameters.py"
  echo "Exiting!"
else
  file=$1
  fileType=${file##*-}
  root=$(echo ${file} | sed -e "s/-${fileType}//")
  if [ ${fileType} == "parameters.py" ]
  then 
    echo $root
    ############################# Classical test for one Test Case ##############################################
    cd Execute
    if [ -d ${root} ]
    then
      rm -Rf $root
    fi
    mkdir -p ${root}/data
    cd $root
    cp ../../../*.py .
    cp ../../Parameters/${file} parameters.py
    python main.py > out_${root}.log 2> out_${root}.err
    mv *.{log,err} ..
    cd ..
    rm -Rf $root
    cd ..
  else
    echo "Unknown file type: " ${fileType}
    echo "Exiting!"
  fi
fi

