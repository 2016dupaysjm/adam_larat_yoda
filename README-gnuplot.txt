launch gnuplot in a terminal

------------------------------------------------------------------------
$ gnuplot

	G N U P L O T
	Version 4.6 patchlevel 5    last modified February 2014
	Build System: Linux x86_64

	Copyright (C) 1986-1993, 1998, 2004, 2007-2014
	Thomas Williams, Colin Kelley and many others

	gnuplot home:     http://www.gnuplot.info
	faq, bugs, etc:   type "help FAQ"
	immediate help:   type "help"  (plot window: hit 'h')

Terminal type set to 'x11'
gnuplot> 
------------------------------------------------------------------------

plot a data file called (format x_i y_i): foo.dat

------------------------------------------------------------------------
gnuplot> plot 'foo.dat'
------------------------------------------------------------------------


GNUPLOT SCRIPTING
gnuplot can be scripted simply by entering gnuplot commande
in a text file (let's say : foo.gplt) and then loading the file

suppose the content of the file foo.gplt is
***
plot bar.dat, foobar.dat
***

one can use:
------------------------------------------------------------------------
gnuplot> plot 'foo.dat'
------------------------------------------------------------------------

SCRIPT : genplot.sh
this is a shell script that scans for all files called c*.dat
and output a line for each of these files that is a plotting
gnuplot commande plus a pause copmmand.
The full output is dumped into a file called
c.gplt

one needs to define a myPause variable in gnuplot that provides the amount of 
time in seconds one should wait between each display

ex: wait for 0.2 s
------------------------------------------------------------------------
gnuplot> myPause=0.2
------------------------------------------------------------------------

ex: wait for the use to hit the return key
------------------------------------------------------------------------
gnuplot> myPause=-1
------------------------------------------------------------------------


One can load this file in gnuplot for showing a animation of the results
------------------------------------------------------------------------
gnuplot> load 'foo.dat'
------------------------------------------------------------------------



