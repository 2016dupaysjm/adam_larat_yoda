# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 16:39:29 2015

@author: larat
"""

import numpy as np
import pylab as pl

pl.close('all')

def detente1(p,vG,pG,cG,gamma=1.4):
    return vG-2.0*cG/(gamma-1)*((p/pG)**(0.5*(gamma-1)/gamma)-1.0)
def choc1(p,vG,pG,cG,gamma=1.4):
    return vG-np.sqrt(2.0/(gamma*pG))*cG*(p-pG)/np.sqrt((gamma-1)*pG+(gamma+1)*p)
def onde1(p,vG,pG,cG,gamma=1.4):
    return (p<pG)*detente1(p,vG,pG,cG,gamma)+(p>=pG)*choc1(p,vG,pG,cG,gamma)
def detente3(p,vD,pD,cD,gamma=1.4):
    return vD+2.0*cD/(gamma-1)*((p/pD)**(0.5*(gamma-1)/gamma)-1.0)
def choc3(p,vD,pD,cD,gamma=1.4):
    return vD+np.sqrt(2/(gamma*pD))*cD*(p-pD)/np.sqrt((gamma-1)*pD+(gamma+1)*p)
def onde3(p,vD,pD,cD,gamma=1.4):
    return (p<pD)*detente3(p,vD,pD,cD,gamma)+(p>=pD)*choc3(p,vD,pD,cD,gamma)

def newton(UG,UD,gamma=1.4):
    vG = UG[1]/UG[0]
    pG = (gamma-1.0)*(UG[2]-0.5*UG[1]*vG)
    cG = np.sqrt(gamma*pG/UG[0])
    vD = UD[1]/UD[0]
    pD = (gamma-1.0)*(UD[2]-0.5*UD[1]*vD)
    cD = np.sqrt(gamma*pD/UD[0])
    p   = 0.5*(pG+pD)
    eps = 1.0e-12
    nIter = 0
    iterMax = 10
    while(nIter<iterMax):
        o1 = onde1(p,vG,pG,cG,gamma)
        o3 = onde3(p,vD,pD,cD,gamma)
        o1prime = (onde1(p+eps,vG,pG,cG,gamma)-o1)/eps
        o3prime = (onde3(p+eps,vD,pD,cD,gamma)-o3)/eps
        deltaP =  (o3-o1)/(o3prime-o1prime)
        p     -= deltaP
        if(abs(deltaP)<eps):
            break
        nIter += 1
    if nIter == iterMax:
        print "Newton failed"
        exit
    ### Wave speeds
    vStar = 0.5*(o1+o3)
    pStar = p
    return pStar,vStar

def dataRiemannEuler(testCase): 
    UG = testCase.Ul
    UD = testCase.Ur
    vStar = testCase.vStar
    pStar = testCase.pStar
    gamma = testCase.gamma
    ### Phase Variables
    vG   = UG[1]/UG[0]
    pG   = (gamma-1.0)*(UG[2]-0.5*UG[1]*vG)
    cG   = np.sqrt(gamma*pG/UG[0])
    rhoG = gamma*pG/cG**2
    VG   = np.array([rhoG,vG,pG,cG])
    vD   = UD[1]/UD[0]
    pD   = (gamma-1.0)*(UD[2]-0.5*UD[1]*vD)
    cD   = np.sqrt(gamma*pD/UD[0])
    rhoD = gamma*pD/cD**2
    VD   = np.array([rhoD,vD,pD,cD])
    ### 1-Wave
    sigmaG = np.zeros(2)
    if pStar>pG: ### Shock, same 1-velocities
        rho1 = rhoG*((gamma+1)*pStar+(gamma-1)*pG)/((gamma-1)*pStar+(gamma+1)*pG)
        c1   = np.sqrt(gamma*pStar/rho1)
        sigmaG[0] = (rhoG*vG-rho1*vStar)/(rhoG-rho1)
        sigmaG[1] = sigmaG[0]
    else:        ### Fan, extreme characteristics
        c1   = cG + 0.5*(gamma-1.0)*(vG-vStar)
        rho1 = rhoG*(pStar/pG)**(1.0/gamma)
        sigmaG[0] = vG-cG
        sigmaG[1] = vStar-c1
    ### 3-Wave
    sigmaD = np.zeros(2)
    if pStar>pD: ### Shock, same 3-velocities
        rho2 = rhoD*((gamma+1)*pStar+(gamma-1)*pD)/((gamma-1)*pStar+(gamma+1)*pD)
        c2   = np.sqrt(gamma*pStar/rho2)
        sigmaD[0] = (rhoD*vD-rho2*vStar)/(rhoD-rho2)
        sigmaD[1] = sigmaD[0]
    else:        ### Fan, extreme characteristics
        c2   = cD - 0.5*(gamma-1.0)*(vD-vStar)
        rho2 = rhoD*(pStar/pD)**(1.0/gamma)
        sigmaD[0] = vStar-c2
        sigmaD[1] = vD-cD    
    V1 = np.array([rho1,vStar,pStar,c1])
    V2 = np.array([rho2,vStar,pStar,c2])
    return VG,sigmaG,V1,V2,sigmaD,VD
    
def solutionRiemannEuler(xi,UG,pStar,vStar,UD,gamma=1.4):
    UExact = np.zeros(np.append(np.shape(xi),3))
    vG   = UG[1]/UG[0]
    pG   = (gamma-1.0)*(UG[2]-0.5*UG[1]*vG)
    cG   = np.sqrt(gamma*pG/UG[0])
    rhoG = gamma*pG/cG**2
    vD   = UD[1]/UD[0]
    pD   = (gamma-1.0)*(UD[2]-0.5*UD[1]*vD)
    cD   = np.sqrt(gamma*pD/UD[0])
    rhoD = gamma*pD/cD**2
    ### 1-Wave
    sigmaG = np.zeros(2)
    if pStar>pG: ### Shock, same 1-velocities
        rho1 = rhoG*((gamma+1)*pStar+(gamma-1)*pG)/((gamma-1)*pStar+(gamma+1)*pG)
        c1   = np.sqrt(gamma*pStar/rho1)
        sigmaG[0] = (rhoG*vG-rho1*vStar)/(rhoG-rho1)
        sigmaG[1] = sigmaG[0]
    else:        ### Fan, extreme characteristics
        c1   = cG + 0.5*(gamma-1.0)*(vG-vStar)
        rho1 = rhoG*(pStar/pG)**(1.0/gamma)
        sigmaG[0] = vG-cG
        sigmaG[1] = vStar-c1
    ### 3-Wave
    sigmaD = np.zeros(2)
    if pStar>pD: ### Shock, same 3-velocities
        rho2 = rhoD*((gamma+1)*pStar+(gamma-1)*pD)/((gamma-1)*pStar+(gamma+1)*pD)
        c2   = np.sqrt(gamma*pStar/rho2)
        sigmaD[0] = (rhoD*vD-rho2*vStar)/(rhoD-rho2)
        sigmaD[1] = sigmaD[0]
    else:        ### Fan, extreme characteristics
        c2   = cD - 0.5*(gamma-1.0)*(vD-vStar)
        rho2 = rhoD*(pStar/pD)**(1.0/gamma)
        sigmaD[0] = vStar-c2
        sigmaD[1] = vD-cD
    v  = ((xi<=sigmaG[0])*vG
         +(xi> sigmaG[0])*(xi< sigmaG[1])*2.0/(gamma+1)*(xi+cG+0.5*(gamma-1.0)*vG)
         +(xi>=sigmaG[1])*(xi<=sigmaD[0])*vStar
         +(xi> sigmaD[0])*(xi< sigmaD[1])*2.0/(gamma+1)*(xi-cD+0.5*(gamma-1.0)*vD)
         +(xi>=sigmaD[1])*vD)
    c  = ((xi<=sigmaG[0])*cG
         +(xi> sigmaG[0])*(xi< sigmaG[1])*(cG+0.5*(gamma-1.0)*(vG-v))
         +(xi>=sigmaG[1])*(xi< vStar)    *c1
         +(xi>=vStar)    *(xi<=sigmaD[0])*c2
         +(xi> sigmaD[0])*(xi< sigmaD[1])*(cD-0.5*(gamma-1.0)*(vD-v))
         +(xi>=sigmaD[1])*cD)
    p  = ((xi<=sigmaG[0])*pG
         +(xi> sigmaG[0])*(xi< sigmaG[1])*pG*(c/cG)**(2.0*gamma/(gamma-1))
         +(xi>=sigmaG[1])*(xi<=sigmaD[0])*pStar
         +(xi> sigmaD[0])*(xi< sigmaD[1])*pD*(c/cD)**(2.0*gamma/(gamma-1))
         +(xi>=sigmaD[1])*pD)
    rho= ((xi<=sigmaG[0])*rhoG
         +(xi> sigmaG[0])*(xi< sigmaG[1])*rhoG*(p/pG)**(1.0/gamma)
         +(xi>=sigmaG[1])*(xi< vStar)    *rho1
         +(xi>=vStar)    *(xi<=sigmaD[0])*rho2
         +(xi> sigmaD[0])*(xi< sigmaD[1])*rhoD*(p/pD)**(1.0/gamma)
         +(xi>=sigmaD[1])*rhoD)
    print "UG = ", UG
    print "pStar = ", pStar, " vStar = ",vStar
    print "rho1  = ", rho1,  " rho2  = ",rho2
    print "c1    = ", c1   , " c2    = ", c2
    print "sigmaG= ", sigmaG
    print "sigmaD= ", sigmaD
    UExact[...,0] = rho
    UExact[...,1] = rho*v
    UExact[...,2] = p/(gamma-1.0)+0.5*rho*v**2
    return UExact
        
def RiemannEuler(testCase):
    testCase.pStar,testCase.vStar = newton(testCase.Ul,testCase.Ur,testCase.gamma)
    return dataRiemannEuler(testCase)

