# -*- coding: utf-8 -*-
"""
Created on Mon Jun 27 18:56:01 2016

@author: larat
"""

import numpy as np
import sys

simuFilename = sys.argv[1]
refFilename  = sys.argv[2]

simu = np.loadtxt(simuFilename)
ref  = np.loadtxt(refFilename)

eps  = 1.0e-10 
nDiffs = np.sum(np.max(np.abs(simu-ref)>eps,axis=1)*1)
print nDiffs
if (nDiffs>0):
  print >> sys.stderr, "*****************************************"
  print >> sys.stderr, "File: ",simuFilename," will fail soon... "
  print >> sys.stderr, "*****************************************"
  print >> sys.stderr, (np.abs(simu-ref)>eps)*(simu-ref)
