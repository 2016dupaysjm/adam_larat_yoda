# -*- coding: utf-8 -*-
"""
Created on Mon Jun 27 18:56:01 2016

@author: larat
"""

import numpy as np
import sys
import codecs
simuFilename = sys.argv[1]
refFilename  = sys.argv[2]

fSimu = codecs.open(simuFilename)
fRef  = codecs.open(refFilename)

simu = np.fromstring(fSimu.read().replace('[','').replace(']',''),sep=' ')
ref  = np.fromstring(fRef.read().replace('[','').replace(']',''),sep=' ')

### Remove unstable schemes
if (min(ref[-3:]))<0.0:
  print 0
else:
  eps  = 1.0e-10
  nDiffs = np.sum((np.abs(simu[:-3]-ref[:-3])>eps)*1)
  print nDiffs
  if (nDiffs>0):
    print >> sys.stderr, "*****************************************"
    print >> sys.stderr, "File: ",simuFilename," will fail soon... "
    print >> sys.stderr, "*****************************************"
    print >> sys.stderr, (np.abs(simu-ref)>eps)*(simu-ref)
