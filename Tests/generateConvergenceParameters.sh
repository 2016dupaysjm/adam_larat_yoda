#!/bin/bash
# Variables are CCC,TTT,BBB,NNN,EEE,FFF,SSS,XXX

for i in {1..3}; 
do 
  if [ $i == 1 ]; 
  then
    EE="linAdv";
    NCNC="20"; 
    FF="localizedCosine";
    FFF="localizedCosine(self)";
    BBB="periodic";
    TTT="1.0";
    NNN="God";
  elif [ $i == 2 ]; 
  then 
    EE="PGD";
    NCNC="20"; 
    FF="rhoProfilePGD";
    FFF="rhoProfilePGD(self)";
    BBB="periodic";
    TTT="1.0";
    NNN="LxF";
  elif [ $i == 3 ]; 
  then 
    EE="burgers";
    NCNC="20"; 
    FF="BurgersSinus";
    FFF="BurgersSinus(self)";
    BBB="periodic";
    TTT="0.1";
    NNN="LxF";
  fi

  ### Forward Euler
  SS="FE";
  SSS="FE()";
  for XXX in {upwind,Godunov,LaxFriedrichs}; 
  do 
    CCC="0.8";
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/God/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
  done
  
  for XXX in {MUSCL,MusclHancock};
  do 
    for NNN in {God,LxF};
    do
      CCC="0.5";
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
    done
  done
  
  XXX="DG2"
  for NNN in {God,LxF};
  do
    CCC="0.3";
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
  done
  
  XXX="DG3"
  for NNN in {God,LxF};
  do
    CCC="0.15";
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
  done
  
  
  ### RK2 Methods
  for SS in {RK2Direct,RK2SSP,RK02SSP};
  do
    if [ ${SS} == RK02SSP ];
    then 
      SSS="RKSSP(2)"
    else
      SSS=${SS}"()"
    fi
    for XXX in {upwind,Godunov,LaxFriedrichs}; 
    do 
      CCC="0.8";
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/God/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
    done
  
    for XXX in {MUSCL,MusclHancock};
    do 
      for NNN in {God,LxF};
      do
        CCC="0.5";
        root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
        echo ${root};
        sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
      done
    done
  
    XXX="DG2"
    for NNN in {God,LxF};
    do
      CCC="0.3";
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
    done
  
    XXX="DG3"
    for NNN in {God,LxF};
    do
      CCC="0.15";
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
    done
  done
  
  ### RK3 Methods
  for SS in {RK3SSP,RK03SSP};
  do
    if [ ${SS} == RK03SSP ];
    then 
      SSS="RKSSP(3)"
    else
      SSS=${SS}"()"
    fi
  
    for XXX in {MUSCL,MusclHancock};
    do 
      for NNN in {God,LxF};
      do
        CCC="0.5";
        root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
        echo ${root};
        sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
      done
    done
  
    XXX="DG2"
    for NNN in {God,LxF};
    do
      CCC="0.3";
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
    done
  
    XXX="DG3"
    for NNN in {God,LxF};
    do
      CCC="0.15";
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
    done
  done 
  
  
  ### RK4 Methods
  SS=RK4
  SSS="RK4()"
  XXX="DG3"
  for NNN in {God,LxF};
  do
    CCC="0.15";
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"convergence.py";
  done


  ### TTGC
  if [ $i -ne 2 ]; 
  then
    SS="FE"
    SSS="FE()"
    XXX="TTGC"
    CCC="0.8"
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/God/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}(self,0.01)/" parametersTests.py > ${root}"convergence.py";
  fi

  ### Arbitrary High Order DG, Namely AHOJ
  if [ $i == 1 ]; 
  then
    FF="localizedCosine10";
    FFF="localizedCosine10(self)";
  elif [ $i == 2 ]; 
  then 
    FF="rhoProfilePGD10";
    FFF="rhoProfilePGD10(self)";
  fi

  SS="FE"
  SSS="FE()"
  XXX="AHOJ"
  CCC="1.0"
  for NNN in {God,LxF};
  do 
    for j in {2..8}
    do     
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}$j${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}(self,$j)/" parametersTests.py > ${root}"convergence.py";
    done
  done

done

### Godunov not implemented for PGD
rm Parameters/PGD*God*-convergence.py Parameters/PGD*upwind-convergence.py
 
