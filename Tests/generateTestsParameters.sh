#!/bin/bash
# Variables are 
#   NCNC = Number of Discretization Cells, 
#   CCC  = CFL Number,
#   BBB  = Boundary Conditions,
#   XXX  = Numerical Scheme General Name,
#   NN and NNN = Numerical Flux,
#   EE and EEE = Selected Equation. Used only to name the test case. In the code the equation is driven by the test case,
#   FF and FFF = Test Case Name, or initial condition if you prefer. See iniFct.py,
#   SS and SSS = Time Integrator,
#   TT and TTT = Final Time,
##############
#
i=0
while [ $i -ge 0 ];
do
  let i=$i+1
  echo $i
  if [ $i == 1 ]; 
  then
    NCNC="20";
    EE="linAdv";
    FF="localizedCosine";
    FFF="localizedCosine(self)";
    BBB="periodic";
    TT="1.0";
  elif [ $i == 2 ];
  then
    NCNC="20";
    EE="linAdv";
    FF="hat";
    FFF="hat(self)";
    BBB="periodic";
    TT="1.0";
  elif [ $i == 3 ];
  then
    NCNC="20";
    EE="linAdv";
    FF="mixed";
    FFF="mixed(self)";
    BBB="periodic";
    TT="1.0";
  elif [ $i == 4 ];
  then
    NCNC="20";
    EE="burgers";
    FF="wave";
    FFF="wave(self)";
    BBB="dirichlet";
    TT="0.2";
  elif [ $i == 5 ]; 
  then
    NCNC="20";
    EE="vecAdv";
    FF="hat_gaussian_periodicCosine";
    FFF="vectorialAdvection(self,('hat','gaussian','periodicCosine'),(1.0,0.5,0.25))"
    BBB="periodic";
    TT="4.0";
  elif [ $i == 6 ]; 
  then
    NCNC="20";
    EE="PGD";
    FF="rhoProfilePGD";
    FFF="rhoProfilePGD(self)";
    BBB="periodic";
    TT="1.0";
  elif [ $i == 7 ]; 
  then
    NCNC="21";
    EE="PGD";
    FF="particlesParcelsPGD";
    FFF="particlesParcelsPGD(self)";
    BBB="dirichlet";
    TT="0.4";
  elif [ $i == 8 ];
  then
    NCNC="10"; 
    EE="burgers";
    FF="BurgersSinus";
    FFF="BurgersSinus(self)";
    BBB="periodic";
    TT="0.1";
  elif [ $i == 9 ];
  then
    NCNC="10"; 
    EE="Euler";
    FF="Sod1";
    FFF="Sod1(self,1.4)";
    BBB="dirichlet";
    TT="0.5";
#####  elif [ $i == 10 ];
#####  then
#####    ### Four moments of the Beta distribution advection at velocity 1.0
#####    NCNC="10"; 
#####    EE="vecAdv";
#####    FF="momentsBeta";
#####    FFF="vectorialAdvection(self,['momentsBeta'],[1.0,1.0,1.0,1.0])";
#####    BBB="periodic";
#####    TT="1.0";
#####  elif [ $i == 11 ];
#####  then
#####    ### Four moments of the Gamma distribution advection at velocity 1.0
#####    NCNC="10"; 
#####    EE="vecAdv";
#####    FF="momentsGamma";
#####    FFF="vectorialAdvection(self,['momentsGamma'],[1.0,1.0,1.0,1.0])";
#####    BBB="periodic";
#####    TT="1.0";
  else
    echo "BREAK"
    break;
  fi

### Forward Euler
  SS="FE";
  SSS="FE()";
  for XXX in {upwind,Godunov,LaxFriedrichs}; 
  do 
    CCC="1.0";
    TTT=${TT}
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/God/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
  done
  
  for XXX in {centered,downwind}; 
  do 
    CCC="1.0";
    TTT="0.1";
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/God/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
  done

  for XXX in {MUSCL,MusclHancock};
  do 
    for NNN in {God,LxF};
    do
      CCC="0.5";
      TTT=${TT}
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
    done
  done

  XXX="DG2"
  for NNN in {God,LxF};
  do
    CCC="0.3";
    TTT=${TT}
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
  done

  XXX="DG3"
  for NNN in {God,LxF};
  do
    CCC="0.15";
    TTT=${TT}
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
  done


### RK2 Methods
  for SS in {RK2Direct,RK2SSP,RK02SSP};
  do
    if [ ${SS} == RK02SSP ];
    then 
      SSS="RKSSP(2)"
    else
      SSS=${SS}"()"
    fi
    for XXX in {upwind,Godunov,LaxFriedrichs}; 
    do 
      CCC="1.0";
      TTT=${TT}
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/God/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
    done
  
    for XXX in {MUSCL,MusclHancock};
    do 
      for NNN in {God,LxF};
      do
        CCC="0.5";
        TTT=${TT}
        root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
        echo ${root};
        sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
      done
    done
  
    XXX="DG2"
    for NNN in {God,LxF};
    do
      CCC="0.3";
      TTT=${TT}
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
    done
  
    XXX="DG3"
    for NNN in {God,LxF};
    do
      CCC="0.15";
      TTT=${TT}
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
    done
  done
  
  ### RK3 Methods
  for SS in {RK3SSP,RK03SSP};
  do
    if [ ${SS} == RK03SSP ];
    then 
      SSS="RKSSP(3)"
    else
      SSS=${SS}"()"
    fi
    for XXX in {centered,downwind}; 
    do 
      CCC="1.0";
      TTT="0.1";
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/God/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
    done
  
    for XXX in {MUSCL,MusclHancock};
    do 
      for NNN in {God,LxF};
      do
        CCC="0.5";
        TTT=${TT}
        root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
        echo ${root};
        sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
      done
    done
  
    XXX="DG2"
    for NNN in {God,LxF};
    do
      CCC="0.3";
      TTT=${TT}
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
    done
  
    XXX="DG3"
    for NNN in {God,LxF};
    do
      CCC="0.15";
      TTT=${TT}
      root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
      echo ${root};
      sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
    done
  done 

  
  ### RK4 Methods
  SS=RK4
  SSS="RK4()"
  for XXX in {centered,downwind}; 
  do 
    CCC="1.0";
    TTT="0.1";
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/God/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
  done
  XXX="DG3"
  for NNN in {God,LxF};
  do
    CCC="0.15";
    TTT=${TT}
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}${NNN}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}()/" parametersTests.py > ${root}"parameters.py";
  done

  ### TTGC
  if [ $i -lt 6 ]; 
  then
    SS="FE"
    SSS="FE()"
    XXX="TTGC"
    CCC="0.8"
    TTT=${TT}
    root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}"-";
    echo ${root};
    sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/God/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}(self,0.01)/" parametersTests.py > ${root}"parameters.py";
  fi
  ### Arbitrary High Order DG, Namely AHOJ
  if [ $i -ne 7 ]; 
  then
    SS="FE"
    SSS="FE()"
    XXX="AHOJ"
    CCC="1.0"
    TTT=${TT}
    for NNN in {God,LxF};
    do 
      for j in {2..8}
      do     
        root=Parameters/${EE}"-"${FF}"-"${SS}"-"${XXX}$j${NNN}"-";
        echo ${root};
        sed -e "s/NCNC/${NCNC}/" -e "s/CCC/${CCC}/" -e "s/TTT/${TTT}/" -e "s/BBB/${BBB}/" -e "s/NNN/${NNN}/"  -e "s/FFF/${FFF}/" -e "s/SSS/${SSS}/" -e "s/XXX/${XXX}(self,$j)/" parametersTests.py > ${root}"parameters.py";
      done
    done
  fi

done  

### PGD does not accept Godunov Flux for the moment
rm Parameters/PGD*God*-parameters.py
### For Burgers, remove FD Schemes 
rm Parameters/burgers-*{upwind,centered,downwind,LaxFriedrichs}*-parameters.py
### For the particlesParcelsPGD test case, also remove FD Schemes
rm Parameters/PGD-particlesParcelsPGD-*{upwind,centered,downwind,LaxFriedrichs}*-parameters.py
### For Euler Test cases, remove Godunov Flux and all FD schemes
rm Parameters/Euler*{God,upwind,centered,downwind}*-parameters.py
   
