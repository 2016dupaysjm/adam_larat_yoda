#!/bin/bash

mkdir -p Parameters Execute/data

./generateConvergenceParameters.sh > Parameters/generatedConvergenceFiles.log
### Copy Code with Execute Directory ###
cp ../*.py Execute/.
cp testConvergence.py Execute/testConvergence.py

cd Parameters
for file in $(ls *-convergence.py | egrep "BurgersSinus.*AHOJ"); 
do 
  root=$(echo ${file} | sed -e 's/-convergence.py//')  
  scheme=$(echo ${file}| cut -d "-" -f 4 | cut -b -4)
  cd ../Execute
  cp ../Parameters/${file} parameters.py
  if [[ ${scheme} = "AHOJ" ]]
  then 
    if [[ ${eq} = "burg" ]]
    ### means we are performing BurgersSinus
    then 
      sed -e "s/MESHES/[5,10,20,40]/"  ../testConvergence.py > testConvergence.py
    else
      sed -e "s/MESHES/[10,20,40,80]/"  ../testConvergence.py > testConvergence.py
    fi
  else 
    sed -e "s/MESHES/[20,40,80,160]/" ../testConvergence.py > testConvergence.py
  fi
  python testConvergence.py > data/Convergence-${root}.log 2> data/Convergence-${root}.err
  rm parameters.py*
  let j=$(cat data/Convergence-${root}.err 2>&1 | wc -l)
  if [ $j -ne 0 ]; 
  then
    echo -e "\e[91;1mFAIL \e[0m" ${root}
    echo "******************"
    echo "cat Convergence-${root}.err"
    echo "******************"
    cat Convergence-${root}.err
  else
    echo -e "\e[92;1mCREATED \e[0m" ${root}
  fi
  cd ../Parameters
done
cd ..

