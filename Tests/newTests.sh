#!/bin/bash
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "           Create New YODA Tests             "
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"

### Generate Tests parameters ###
if [ -d "Parameters" ];
then 
  echo "********************"
  echo "rm -Rf Parameters"
  echo "********************"
  rm -Rf Parameters
fi
mkdir Parameters
./generateTestsParameters.sh > Parameters/generatedParameterFiles.log

### Make directory for data and log files ###
if [ -d "Execute" ];
then 
  echo "********************"
  echo "rm -Rf Execute"
  echo "********************"
  rm -Rf Execute
fi
mkdir -p Execute/data

### Copy Code with Execute Directory ###
cp ../*.py Execute/.
cp callMain.py Execute/.

cd Parameters
for file in $(ls vecAdv-momentsGamma*-parameters.py); 
do 
  root=$(echo ${file} | sed -e 's/-parameters.py//')
  cd ../Execute
  cp ../Parameters/${file} parameters.py
  python callMain.py > ${root}.log 2> ${root}.err
  rm parameters.py*
  let j=$(cat ${root}.err 2>&1 | wc -l)
  if [ $j -ne 0 ]; 
  then
    echo -e "\e[91;1mFAIL \e[0m" ${root}
    echo "******************"
    echo "cat ${root}.err"
    echo "******************"
    cat ${root}.err
  else
    echo -e "\e[92;1mCREATED \e[0m" ${root}
  fi
  cd ../Parameters
done
cd ..

### Check if log files contains exactly 6 lines
let j=$(./checkLogFiles.sh 2>&1 | wc -l)
if [ $j -ne 0 ]; 
then
  echo "******************"
  echo -e "Check Log Files... \e[91;1mFAIL \e[0m"
  echo "******************"
else
  echo "******************"
  echo -e "Check Log Files... \e[92;1mPASS \e[0m"
  echo "******************"
fi

#./newConvergenceTest.sh

