#!/bin/bash

if [ $# -ne 1 ]
then
  echo "This script needs one argument, the name of the test file"
  echo "Syntax:"
  echo "  If one wants to run a simple test case: runOneTest.sh root-parameters.py"
  echo "  If one wants to run a convergence case: runOneTest.sh root-convergence.py"
  echo "Exiting!"
else
  file=$1
  fileType=${file##*-}
  root=$(echo ${file} | sed -e "s/-${fileType}//")
  if [ ${fileType} == "parameters.py" ]
  then 
    ############################# Classical test for one Test Case ##############################################
    cd Execute
    if [ -d ${root} ]
    then
      rm -Rf $root
    fi
    mkdir -p ${root}/data
    cd $root
    cp ../../../*.py .
    cp ../../Parameters/${file} parameters.py
    python main.py > ${root}.log 2> ${root}.err
    mv data/* ../data/.
    mv *.{log,err} ..
    cd ..
    rm -Rf $root
    (( j = $(python ../diff.py data/${root}-001.dat ../data/${root}-001.dat)+$(cat ${root}.err 2>&1 | wc -l) ))
    if [ $j -ne 0 ]; 
    then
      echo -e $'\e[91;1m''FAIL '$'\e[0m' ${root}
      echo "******************"
      echo "diff data/${root}-001.dat ../../data/${root}-001.dat"
      echo "******************"
      diff data/${root}-001.dat ../data/${root}-001.dat
      echo "******************"
      echo "cat ${root}.err"
      echo "******************"
      cat ${root}.err
    else
      echo -e $'\e[92;1m''PASS '$'\e[0m' ${root}
    fi
    cd ..
  elif [ ${fileType} == "convergence.py" ]
  then
    ############################# Convergence Test Case ##############################################
    scheme=$(echo ${file}| cut -d "-" -f 4 | cut -b -4)
    cd Execute
    mkdir -p Convergence-${root}/data
    cd Convergence-${root}
    cp ../../../*.py .
    cp ../../Parameters/${file} parameters.py
    if [[ ${scheme} = "AHOJ" ]]
    then 
      if [[ ${eq} = "burg" ]] 
      ### means we are performing BurgersSinus
      then 
        sed -e "s/MESHES/[5,10,20,40]/"  ../../testConvergence.py > testConvergence.py
      else
        sed -e "s/MESHES/[10,20,40,80]/"  ../../testConvergence.py > testConvergence.py
      fi
    else 
      sed -e "s/MESHES/[20,40,80,160]/" ../../testConvergence.py > testConvergence.py
    fi
    python testConvergence.py > data/Convergence-${root}.log 2> data/Convergence-${root}.err
    mv data/* ../data
    cd ..
    rm -Rf Convergence-${root}
    (( j = $(python ../diffConvergence.py data/Convergence-${root}.log ../data/Convergence-${root}.log)+$(cat data/Convergence-${root}.err 2>&1 | wc -l) ))
    if [ $j -ne 0 ]; 
    then
      echo -e $'\e[91;1m''FAIL '$'\e[0m' ${root}
      echo "******************"
      echo "diff data/Convergence-${root}.log ../data/Convergence-${root}.log" 
      echo "******************"
      diff data/Convergence-${root}.log ../data/Convergence-${root}.log
      echo "******************"
      echo "cat Convergence-${root}.err"
      echo "******************"
      cat data/Convergence-${root}.err
    else
      echo -e $'\e[92;1m''PASS '$'\e[0m' ${root}
    fi
    cd ..
  else
    echo "Unknown file type: " ${fileType}
    echo "Exiting!"
  fi
fi

