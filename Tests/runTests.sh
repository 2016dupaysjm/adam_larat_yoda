#!/bin/bash
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "            Start Testing YODA               "
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"

### Make directory for data and log files ###
if [ -d "Execute" ];
then 
  echo "********************"
  echo "rm -Rf Execute"
  echo "********************"
  rm -Rf Execute
fi
mkdir -p Execute/data

####if [ $# -eq 1 ]
####then
make generateParameters  
make single -j$1
make generateConvergence  
make convergence -j$1
####else 
####  make single
####  make convergence
####fi

### Check if log files contains exactly 6 lines
(( j = $(./checkLogFiles.sh 2>&1 | wc -l) ))
if [ $j -ne 0 ]; 
then
  echo "******************"
  echo -e "Check Log Files... "$'\e[91;1m''FAIL '$'\e[0m'
  echo "******************"
else
  echo "******************"
  echo -e "Check Log Files... "$'\e[92;1m''PASS '$'\e[0m'
  echo "******************"
fi

