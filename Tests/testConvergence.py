# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt 
from main import Main

plt.close("all")
meshList = np.array(MESHES)
nbMeshes = np.size(meshList)
Err      = np.zeros((nbMeshes,4))    
for i in np.arange(nbMeshes):
    Err[i,:] = Main(meshList[i],False)
dx    = Err[:,0]
ErrL1 = Err[:,1]
ErrL2 = Err[:,2]
ErrLinf = Err[:,3]
[a1,b1] = np.polyfit(np.log(dx),np.log(ErrL1),1)
[a2,b2] = np.polyfit(np.log(dx),np.log(ErrL2),1)
[a3,b3] = np.polyfit(np.log(dx),np.log(ErrLinf),1)
print Err
print a1, a2, a3

