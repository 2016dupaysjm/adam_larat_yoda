#!/bin/bash

OK=$(grep  "PASS" run.log 2>&1 | grep -v "Log Files" 2>&1 | wc -l)
KO=$(grep "FAIL" run.log 2>&1 | wc -l)
TOT=$(ls -l data/* 2>&1 | wc -l)

echo -e ${OK}" Tests have \e[92;1mPASSED\e[0m"
echo -e ${KO}" Tests have \e[91;1mFAILED\e[0m"
echo -e ${TOT}" Files have been found in ./data"

