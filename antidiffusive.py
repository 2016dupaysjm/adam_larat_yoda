#!/usr/bin/env python

import miscRoutines1D as mR
import mesh1D as mE1D
import numpy as np
import InitFct as IF
import pdb

half = 0
nbDigit = 3  # nb of digits for numbering the output files name 
ng = 2  # nb of ghost cells on each side of the domain
CFL = 0.45



print("Compute: begin")
t = 0
xMin = 0.0
xMax = 1.0
nx = 300    
tMax = 6.0
nbSaveMax = 300  # nb of data output between t=0 and t=tMax

# create the good variables!
# mesh, velocity, variable, x_j
m = mE1D.Mesh1D(nx, ng, xMin, xMax)
x = m.GetCoord()
c = m.NewCellArray()
eu = m.NewEdgeArray()
ec = m.NewEdgeArray()
u = m.NewCellArray()

emc = m.NewEdgeArray()
eMc = m.NewEdgeArray()
aPlus = m.NewEdgeArray()
aMinus = m.NewEdgeArray()
APlus = m.NewEdgeArray()
AMinus = m.NewEdgeArray()

omegaPlus = m.NewEdgeArray()
omegaMinus = m.NewEdgeArray()
OmegaPlus = m.NewEdgeArray()
OmegaMinus = m.NewEdgeArray()


# Compute Velocity Field
# u_j = uBar
uBar = 1.2
J = m.GetInnerCellId()
u[J] = uBar
# u[J] = np.cos(6*np.pi * x[J])
mE1D.PeriodicBC(m, u)

# apply boundary condition

# u_j+1/2 = (u_j + u_{j+1})/2
J = m.GetAllEdgeId()
eu[J + half] = (u[J] + u[J + 1]) / 2.0

# compute the initial condition
# c_j = f(x_j), j = inner cell id
J = m.GetInnerCellId()
c[J] = np.cos(2*np.pi*x[J])
#c[J] = (0.25 < x[J]) * (x[J] < 0.75) 


nbStep = 0
n = 0  # snapshot number for the data
# save the initial condition
I = m.GetInnerCellId()
fileName = "data/c" + str(n).zfill(nbDigit) + ".dat"
mR.SaveData(fileName, c[I], x[I])

dumb = 0.0

#########" SPACE STEP!!
######## IS CALLED m.dx


# time loop: begin
while t < tMax :
    # set periodic BC
    mE1D.PeriodicBC(m, c)

    # Compute time step
    # dt such that dt = CFL * dx/  max|u_{j+1/2}| 
    # CFL = coef between 0 and 1
    dt = CFL * m.dx / np.max(np.abs(eu))
    
    # Compute fluxes

    #---------------------- UPWIND    
    # c_{j+1/2} = ... j +1/2 = inner edges
    # theta_{j+1/2} = (u_{j+1/2} > 0)
    # c_{j+1/2} = c_j     if     theta_{j+1/2}
    #           = c_{j+1} if not theta_{j+1/2} 

#     J = m.GetInnerEdgeId()
#     theta = (eu[J + half] > 0)
#     print theta
    
#     ec[J + half] = theta * c[J] + (~theta) * c[J + 1]

    # ANTI-DIFFUSIVE FLUX

    # compute m_{j+1/2} and M_{j+1/2}
    J = m.GetAllEdgeId()
    emc[J + half] = np.minimum(c[J], c[J + 1])
    eMc[J + half] = np.maximum(c[J], c[J + 1])
    
    sigma = dt / m.dx
    
    J = m.GetInnerEdgeId()
    # for u_{j+1/2} > 0
    # aPlus__{j+1/2} APlus_{j+1/2}
    aPlus[J + half] = (
        c[J] + (eMc[J + half - 1] - c[J]) * (
                            eu[J + half - 1] / eu[J + half] 
                            - 1.0 / (eu[J + half] * sigma)
                                    )
                     )

    APlus[J + half] = (
        c[J] + (emc[J + half - 1] - c[J]) * (
                            eu[J + half - 1] / eu[J + half] 
                            - 1.0 / (eu[J + half] * sigma)
                                    )
                     )
    J = m.GetInnerEdgeId()

    # for u_{j+1/2} < 0
    # aMinus_{j+1/2} AMinus_{j+1/2}
    aMinus[J + half] = (
    c[J + 1] + (eMc[J + half + 1] - c[J + 1]) * (
                        eu[J + half + 1] / eu[J + half] 
                        + 1.0 / (eu[J + half] * sigma)
                                )
                 )

    AMinus[J + half] = (
    c[J + 1] + (emc[J + half + 1] - c[J + 1]) * (
                        eu[J + half + 1] / eu[J + half] 
                        + 1.0 / (eu[J + half] * sigma)
                                )
                 )
    
    # for u_{j+1/2} > 0
    # compute omegaPlus_{j+1/2} and OmegaPlus_{j+1/2}  
    # [omegaPlus_{j+1/2}, OmegaPlus_{j+1/2}] 
    # =  ( m_{j+1/2} and M_{j+1/2} ) intersect with [aPlus[J + half], APlus[J + half]
    # omegaPlus = max(m, aPlus)
    # OmegaPlus = min(M, Aplus)
    omegaPlus[J + half] = np.maximum(emc[J + half], aPlus[J + half])
    OmegaPlus[J + half] = np.minimum(eMc[J + half], APlus[J + half])
    
    
    # for u_{j+1/2} < 0
    # compute omegaMinus_{j+1/2} and OmegaMinus_{j+1/2} 
    # omegaMinus = max(m, aMinus)
    # OmegaMinus = min(M, AMinus)
    omegaMinus[J + half] = np.maximum(emc[J + half], aMinus[J + half])
    OmegaMinus[J + half] = np.minimum(eMc[J + half], AMinus[J + half])
    
    limitedPlus = np.maximum(omegaPlus[J+half], np.minimum(c[J+1], OmegaPlus[J+half]))
    limitedMinus = np.maximum(omegaMinus[J+half], np.minimum(c[J], OmegaMinus[J+half]))
    upwindPlus = c[J]
    upwindMinus = c[J+1] 

    # Compute the flux
    ec[J + half] = (
     (eu[J + half] > 0) * (eu[J + half - 1] > 0)*limitedPlus                                        
    +(eu[J + half] > 0) * (eu[J + half - 1] < 0)*upwindPlus
    +(eu[J + half] < 0) * (eu[J + half + 1] < 0)*limitedMinus
    +(eu[J + half] < 0) * (eu[J + half + 1] > 0)*upwindMinus
                  )
    

    # update cell values
    # c_j^{new} = c_j - (dt/dx)[cu_{j+1/2} - cu_{j-1/2}]
    #                 + c_j*(dt/dx)*[u_{j+1/2} - u_{j-1/2}]
    # j = all inner cells[cu_{j+1/2} - cu_{j-1/2}]
    J = m.GetInnerCellId()
    c[J] = (
            c[J] - (dt / m.dx) * (
                             eu[J + half] * ec[J + half]
                            - 
                             eu[J + half - 1] * ec[J + half - 1]
                             ) 
            + (dt / m.dx) * c[J] * (
                             eu[J + half]
                            - 
                             eu[J + half - 1]
                             )
            )    
    
    mR.PrintInfoLine(t, n, dt, tMax, nbSaveMax, nbStep)

    # increment stuff
    nbStep = nbStep + 1
    t = t + dt
#    t = tMax + 1  # I REALLY NEED TO ERASE THIS LINE

    # save if needed
    if mR.INeedToSave(t, n, tMax, nbSaveMax) :
        n = n + 1
        fileName = "data/c" + str(n).zfill(nbDigit) + ".dat"
        mR.SaveData(fileName, c[I], x[I])    

# time loop: end

print("Compute: done.")













