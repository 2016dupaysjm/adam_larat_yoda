# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt 
from main import Main

plt.close("all")
meshList = np.array([20,40,80,160,320,640])
nbMeshes = np.size(meshList)
Err      = np.zeros((nbMeshes,4))    
for i in np.arange(nbMeshes):
    print "*** %4d ***" % meshList[i]
    Err[i,:] = Main(meshList[i],False)
dx    = Err[:,0]
ErrL1 = Err[:,1]
ErrL2 = Err[:,2]
ErrLinf = Err[:,3]
[a1,b1] = np.polyfit(np.log(dx),np.log(ErrL1),1)
[a2,b2] = np.polyfit(np.log(dx),np.log(ErrL2),1)
[a3,b3] = np.polyfit(np.log(dx),np.log(ErrLinf),1)
plt.plot(dx,ErrL1,'r+',label="",markersize=20)
plt.plot(dx,np.exp(b1)*dx**a1,'r-',label="L1Err, slope=%.2f"%a1,linewidth=3)
plt.plot(dx,ErrL2,'gx',label="",markersize=20)
plt.plot(dx,np.exp(b2)*dx**a2,'g-',label="L2Err, slope=%.2f"%a2,linewidth=3)
plt.plot(dx,ErrLinf,'bx',label="",markersize=20)
plt.plot(dx,np.exp(b3)*dx**a3,'b-',label="LinfErr, slope=%.2f"%a3,linewidth=3)
plt.xscale('log'); plt.yscale('log')
plt.legend(loc='upper left')
print Err
print a1, a2, a3

