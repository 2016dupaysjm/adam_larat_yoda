#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  4 15:48:46 2018

@author: larat
"""

import numpy as np
import pylab as p

p.close('all')
rootname = '/home/larat/Codes/Spyder/YODA/data/momentsAdv-momentsBorder-RK3SSP-DG3LxF-'

for i in np.arange(11):
    filename = rootname+'%03d'%i+'.dat'
    U = np.loadtxt(filename)
    with open(filename, 'r') as f:
        header = f.readline()
    Nmom = (np.shape(U)[-1]-1)/2
    X = U[:,0]
    ULim = U[:,1:Nmom+1]
    UExact = U[:,Nmom+1:]
    p.figure()
    p.plot(X,np.abs(ULim-UExact),label="L1Error")
    p.xlabel('x')
    p.ylabel('L1Error')
    p.title(header)
    p.legend()
    p.savefig(rootname+"L1Error-"+"%03d"%i+'.png')
    p.close()
#U000 = np.loadtxt('/home/larat/Codes/Spyder/YODA/data/momentsAdv-momentsBorder-RK3SSP-DG3LxF-000.dat')
#U001 = np.loadtxt('/home/larat/Codes/Spyder/YODA/data/momentsAdv-momentsBorder-RK3SSP-DG3LxF-001.dat')
##U100 = np.loadtxt('/home/larat/Codes/Spyder/YODA/data/momentsAdv-momentsBorder-RK3SSP-DG3LxF-100.dat')
#
#X         = U000[:,0]
#U000Lim   = U000[:,1:9]
#U000Exact = U000[:,9:]
#U001Lim   = U001[:,1:9]
#U001Exact = U001[:,9:]
##U100Lim   = U100[:,1:9]
##U100Exact = U100[:,9:]
#
#p.plot(X,np.abs(U000Lim-U000Exact),label="L1Errors")
#p.legend()
#p.figure()
#p.plot(X,np.abs(U001Lim-U001Exact),label="L1Errors")
#p.legend()
##p.figure()
##p.plot(X,np.abs(U100Lim-U100Exact),label="L1Errors")
##p.legend()