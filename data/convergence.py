# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 18:23:14 2016

@author: larat
"""

import sys
import numpy as np
from scipy import stats
import pylab as plt

plt.close('all')
if(len(sys.argv)==1):
    print "Usage: convergence.py rootName"
    rootName = "Convergence.dat"
    print "Since you have provided none, I have provided the arguments for you:"
    print rootName
elif(len(sys.argv)==2):
    rootName = str(sys.argv[1])
else:
    print "Usage: convergence.py rootName"
    sys.exit(-1)

### Must contain on each line : N m1_L1 ... mk_L1 m1_L2 ... mk_L2 m1_Linf ... mk_Linf
conv = np.loadtxt(rootName)

nMom = (np.shape(conv)[-1]-1)/3
NMin = np.min(conv[:,0])
NMax = np.max(conv[:,0])
lconv = np.log(conv)
x     = np.linspace(NMin,NMax,10)
for i in np.arange(nMom): 
    Slope1, inter1, r1, p1, err1 = stats.linregress(lconv[:,0],lconv[:,1+i])
    Slope2, inter2, r2, p2, err2 = stats.linregress(lconv[:,0],lconv[:,1+nMom+i])
    SlopeInf, interInf, rInf, pInf, errInf = stats.linregress(lconv[:,0],lconv[:,1+2*nMom+i])
    plt.figure()
    plt.loglog(conv[:,0],conv[:,1+i],'r+',ms=16,label='L1 Error')
    plt.loglog(x,np.exp(Slope1*np.log(x)+inter1),'r-',lw=3,label="Slope = "+("%.2f"%Slope1))
    plt.loglog(conv[:,0],conv[:,1+nMom+i],'bx',ms=16,label='L2 Error')
    plt.loglog(x,np.exp(Slope2*np.log(x)+inter2),'b-',lw=3,label="Slope = "+("%.2f"%Slope2))
    plt.loglog(conv[:,0],conv[:,1+2*nMom+i],'gd',ms=16,label='LInf Error')
    plt.loglog(x,np.exp(SlopeInf*np.log(x)+interInf),'g-',lw=3,label="Slope = "+("%.2f"%SlopeInf))
    plt.ylabel('Errors')
    plt.xlabel('# Cells')
    plt.title('Moment '+str(i))    
    plt.legend()
    plt.savefig(rootName[:-4]+'_M'+str(i)+'.png')

