#!/bin/bash
for T in {Beta,Gamma,Border}
do 
  for O in {1,2,3}
  do 
    rootname="momentsAdv-moments${T}_Order${O}"
    echo ${rootname}
    cat out_${rootname}.log | grep -e ^[NL] | grep -v "Norm" | grep -v "Loss" | sed -e 's/inf/i/'| sed -e ':z;N;s/\]*\nL[1|2|i] Error =  \[//;bz' | sed -e 's/Number of Cells: N =  //' -e 's/\]$//' > Convergence_${rootname}.dat
    python convergence.py Convergence_${rootname}.dat
    python convergencePerNorm.py Convergence_${rootname}.dat
  done
done

