#!/bin/bash
for T in {Beta,Gamma,Border}
do 
  for O in {1,2,3}
  do 
    for N in {32,64,128,256,512} #{40,80,100,200,400} 
    do 
      echo "***********************************"
      echo "** T = "$T", O = "$O", N = "$N"  **"
      echo "***********************************"
      sed -e "s/NNN/${N}/" -e "s/TTT/${T}/" parametersNT${O}.py > ../parameters.py
      sleep 1
      cd ..; 
      python main.py
      cd data
    done | tee out_momentsAdv-moments${T}_Order${O}.log
  done
done
