#!/bin/bash
for O in {2,3}
do
  cd Order${O} 
  for T in {Beta,Gamma,Border}
  do 
    echo "Compiling Order${O}/Convergence${T}Order${O}_DifferentMoments.tex"
    echo "Once..."
    pdflatex Convergence${T}Order${O}_DifferentMoments.tex #| grep -i warning
    echo "Twice..."
    pdflatex Convergence${T}Order${O}_DifferentMoments.tex #| grep -i warning
  done
  cd ..
done

