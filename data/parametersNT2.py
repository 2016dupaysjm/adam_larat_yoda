# -*- coding: utf-8 -*-
import timeStepping
import schemes
import initFct
#import equations


class parameters:
    def __init__(self,N=-1):
        self.half    = 0  ### For the sake of consistency with the math notations
        self.nbDigit = 3  ### nb of digits for numbering the output files name 
        ### Mesh Quantities
        NCells       = NNN
        if(N<0):
            self.N = NCells   
        else: 
            self.N = N
        self.ng      = 1  ### nb of ghost cells on each side of the domain
        self.NTot    = self.N+2*self.ng
        self.xMin    = 0.0
        self.xMax    = 1.0
        self.dx      = (self.xMax-self.xMin)/self.N
        ### Time Quantities
        self.CFL       = 0.3
        self.tMax      = 1.0
        self.nbSaveMax = 10

        """ *** Boundary Conditions *** 
            periodic,dirichlet,...
            See mesh1D.py..."""
        self.BCs   = "periodic" 

        """ *** Numerical Flux ***
            FStar (Godunov or Exact), LaxFriedrichs, LaxWendroff,...
            see equations.py..."""
        self.numFlux = "LxF"

        """ *** Initial Condition ***
            cos4x, gaussian, hat, periodicCosine, localizedCosine, mixed
            see initFct.py..."""
        self.initFunction = initFct.momentsAdvection(self,'TTT',8)

        """ *** Equation to be solved *** 
            Now instantiated by initFunction. 
            see initFct.py """
        self.equation = self.initFunction.getEquation()

        """ *** ODE integrator ***  
            FE, RK2Direct, RK2SSP
            See timeStepping.py...""" 
        self.timeIntegration = timeStepping.RKSSP(2)

        """ *** Semi-discretization in space *** 
            upwind, centered, downwind, Godunov, LaxFriedrichs, MUSCL, MusclHancock,
            DG2, DG3
            See schemes.py..."""       
        self.scheme = schemes.DG2()#upwind()     

        ### Computation rootName
        self.rootName = ("data/"
                        +self.equation.name()+"-"
                        +self.initFunction.name()+"-"
                        +self.timeIntegration.name()+"-"
                        +self.scheme.name()+(self.numFlux if self.scheme.usesFStar() else "") +"-")
                        
        ### Computation Variables
        self.t      = 0.0
        self.n      = 0
        self.nbStep = 0
        self.dt     = 0.0
        self.mass0  = 0.0
