for file in `ls $1*.dat`; 
do 
  ### root contain filename without extension
  root=$(echo ${file} | sed -e 's/.dat//')
  ### final time is read from first line of the file
  time=$(head -n 1 ${file} | cut -b 20-)
  ### equation name is read from the first 4 caracters of the file name
  eq=$(echo ${file}| cut -b -4)
  ### name of the test case is read from the second item of the file name
  testCase=$(echo ${file}|cut -d "-" -f 2)
  ### Scheme name is read from the third and fourth item of the file name
  scheme=$(echo ${file}| cut -d "-" -f 3-4)
  ### Common Format for Gnuplotting
  format="set style line 3 lw 5 lc 1 lt 1 ps 2; set style line 4 lw 5 lc -1 lt 1 ps 2; set terminal png font 'Helvetica,18' size 1200,800; set palette rgbformulae 22,13,-31; set title 't=${time}'; set xlabel 'x'; "
  echo ${root} ${time} ${eq} ${testCase} ${scheme}
  if [ ${eq} == "linA" ] || [ ${eq} == "burg" ]; 
  then
    if [ ${testCase} == "BurgersSinus" ] 
    then
      gnuplot -e "${format} set ylabel 'u'; plot '${file}' u 1:3 w lp ls 4 t 'Exact', '${file}' u 1:2 w lp ls 3 t '${scheme}'" > ${root}.png
    else
      gnuplot -e "${format} set ylabel 'u'; plot '${file}' u 1:2 w lp ls 3 t '${scheme}'" > ${root}.png
    fi
  elif [ ${eq} == "vecA" ] || [ ${eq} == "mome" ];
  then
    ### get each init function name, separated by white spaces
    inits=$(echo ${file} | cut -d "-" -f 2 | sed -e 's/_/ /g')
    ### if we have been simulating moments transport, get number of moments and replace inits by them
    #if [ $(echo ${file} | cut -d "-" -f 2 | cut -c1-7) == "moments" ]
    if [ $(echo ${inits} | cut -c1-7 | sed -e 's/ *//g') == "moments" ];
    then
      cols=$(tail ${file} -n1 | cut -d ' ' -f2-)
      inits=""
      counter=0
      for i in ${cols};
      do 
        inits=${inits}" m"${counter}
        let counter=${counter}+1
      done
    fi
    plotCommand="plot "
    ### plotted column starts at 2, so initial counter is 1
    counter=1
    for init in ${inits};
    do
      let counter=${counter}+1
      if [ ${counter} -ne 2 ];
      then
        plotCommand=${plotCommand}", "
      fi
      plotCommand=${plotCommand}" '${file}' u 1:${counter} w lp lw 6 ps 2 t '${init}'" #-${scheme}'"
      gnuplot -e "${format} set ylabel 'solutions'; set yrange [0:1.1]; ${plotCommand}" > ${root}.png
    done
  elif [ ${eq} == "PGD-" ]; 
  then
    gnuplot -e "${format} set ylabel 'rho'; plot '${file}' u 1:2 w lp ls 3 t '${scheme}'" > ${root}-rho.png
    gnuplot -e "${format} set ylabel 'rhoU'; plot '${file}' u 1:3 w lp ls 3 t '${scheme}'" > ${root}-rhoU.png
    gnuplot -e "${format} set ylabel 'u'; plot '${file}' u 1:(\$3/\$2) w lp ls 3 t '${scheme}'" > ${root}-u.png
  elif [ ${eq} == "Eule" ];
  then
    echo ${testCase}
    if [ ${testCase} == "Sod1" ]
    then
      gnuplot -e "${format} set ylabel 'rho';  plot '${file}' u 1:5 w l ls 4 t 'Exact', '${file}' u 1:2 w lp ls 3 t '${scheme}'" > ${root}-rho.png
      gnuplot -e "${format} set ylabel 'rhoU'; plot '${file}' u 1:6 w l ls 4 t 'Exact', '${file}' u 1:3 w lp ls 3 t '${scheme}'" > ${root}-rhoU.png
      gnuplot -e "${format} set ylabel 'rhoE'; plot '${file}' u 1:7 w l ls 4 t 'Exact', '${file}' u 1:4 w lp ls 3 t '${scheme}'" > ${root}-rhoE.png
      gnuplot -e "${format} set ylabel 'u'; plot '${file}' u 1:(\$6/\$5) w l ls 4 t 'Exact', '${file}' u 1:(\$3/\$2) w lp ls 3 t '${scheme}'" > ${root}-u.png
      gnuplot -e "${format} set ylabel 'P'; plot '${file}' u 1:(0.4*((\$7)-0.5*((\$6)*(\$6))/(\$5))) w l ls 4 t 'Exact', '${file}' u 1:(0.4*((\$4)-0.5*((\$3)*(\$3))/(\$2))) w lp ls 3 t '${scheme}'" > ${root}-pressure.png
    elif [ ${testCase} == "axisymmetric" ]
    then
      gnuplot -e "${format} set ylabel 'r-rho'; plot '${file}' u 1:2 w lp ls 3 t '${scheme}'" > ${root}-r-rho.png
      gnuplot -e "${format} set ylabel 'rho'; plot '${file}' u 1:(\$2/\$1) w lp ls 3 t '${scheme}'" > ${root}-rho.png
      gnuplot -e "${format} set ylabel 'r-rhoU'; plot '${file}' u 1:3 w lp ls 3 t '${scheme}'" > ${root}-r-rhoU.png
      gnuplot -e "${format} set ylabel 'r-rhoE'; plot '${file}' u 1:4 w lp ls 3 t '${scheme}'" > ${root}-r-rhoE.png
      gnuplot -e "${format} set ylabel 'u'; plot '${file}' u 1:(\$3/\$2) w lp ls 3 t '${scheme}'" > ${root}-u.png
      gnuplot -e "${format} set ylabel 'r-P'; plot '${file}' u 1:(0.4*((\$4)-0.5*((\$3)*(\$3))/(\$2))) w lp ls 3 t '${scheme}'" > ${root}-r-pressure.png
      gnuplot -e "${format} set ylabel 'P'; plot '${file}' u 1:((0.4*((\$4)-0.5*((\$3)*(\$3))/(\$2)))/\$1) w lp ls 3 t '${scheme}'" > ${root}-pressure.png
    else 
      gnuplot -e "${format} set ylabel 'rho'; plot '${file}' u 1:2 w lp ls 3 t '${scheme}'" > ${root}-rho.png
      gnuplot -e "${format} set ylabel 'rhoU'; plot '${file}' u 1:3 w lp ls 3 t '${scheme}'" > ${root}-rhoU.png
      gnuplot -e "${format} set ylabel 'rhoE'; plot '${file}' u 1:4 w lp ls 3 t '${scheme}'" > ${root}-rhoE.png
      gnuplot -e "${format} set ylabel 'u'; plot '${file}' u 1:(\$3/\$2) w lp ls 3 t '${scheme}'" > ${root}-u.png
      gnuplot -e "${format} set ylabel 'P'; plot '${file}' u 1:(0.4*((\$4)-0.5*((\$3)*(\$3))/(\$2))) w lp ls 3 t '${scheme}'" > ${root}-pressure.png
    fi
  else
    echo "Unknown equation type" ${eq}
    echo "Abort"
    exit
  fi
done
