# -*- coding: utf-8 -*-
"""
Created on Tue Oct  4 16:11:16 2016

@author: larat
"""


import sys
import numpy as np
import pylab as plt

plt.close('all')
if(len(sys.argv)==1):
    print "Usage: plotReducedMoments.py rootName nDofs"
    rootName = "momentsAdv-momentsGamma-RK2SSP-DG2LxF-001.dat"
    nDofs    = 2
    print "Since you have provided none, I have provided the arguments for you:"
    print rootName, nDofs
elif(len(sys.argv)==3):
    rootName = str(sys.argv[1])
    nDofs    = int(sys.argv[2])
else:
    print "Usage: plotReducedMoments.py rootName nDofs"
    sys.exit(-1)

f    = open(rootName)
time = float(f.readline().strip().split(' ')[-1])

mBar = np.loadtxt(rootName)
m    = np.zeros((nDofs+1,)+np.shape(mBar))
mCopy= np.copy(m)
mCopy[0] = mBar
nMom = (np.shape(mBar)[-1]-1)/2
mBar[:,1]      = np.where(mBar[:,1]==0.0,-1,mBar[:,1])
mBar[...,2:nMom+1] /= mBar[:,1,np.newaxis]
mBar[:,1+nMom] = np.where(mBar[:,1+nMom]==0.0,-1,mBar[:,1+nMom])
mBar[...,2+nMom: ] /= mBar[:,nMom+1,np.newaxis]
for i in np.arange(nDofs):
    m[i+1]     = np.loadtxt(rootName+str(i))
    mCopy[i+1] = m[i+1]
    m[i+1,:,1] = np.where(m[i+1,:,1]==0.0,-1,m[i+1,:,1])
    m[i+1,...,2:nMom+1] /= m[i+1,:,1,np.newaxis]
    m[i+1,:,1+nMom] = np.where(m[i+1,:,1+nMom]==0.0,-1,m[i+1,:,1+nMom])
    m[i+1,...,nMom+2:]  /= m[i+1,:,1+nMom,np.newaxis]
m[0,...] = mBar

### Now plot
I = np.array([1,0,2]) ### Dofs order
#for x in np.arange(np.shape(mBar)[0]):
#    #if((m[0,x,0]>=1.0/3.0) & (m[0,x,0]<=2.0/3.0) ):
#        plt.plot(m[I,x,2+nMom],m[I,x,3+nMom],'g+-',ms=16,lw=3)
#        plt.plot(m[I,x,2],m[I,x,3],'rx-',ms=12)
#plt.plot(mBar[:,2+nMom],mBar[:,3+nMom],'go',label="Exact",ms=18)
plt.plot(mBar[1::2,2],mBar[1::2,3],'ro-',label="Mean Values",ms=8)
#m1Min,m1Max = plt.xlim()
m1Min = 0.0;m1Max = 0.6
m1 = np.linspace(m1Min,m1Max,1000)
plt.plot(m1,m1**2,'k-',lw=5,label="Border")
plt.xlabel('m1/m0')
plt.ylabel('m2/m0')
plt.title("time = "+str(time))
plt.legend()
#plt.xlim([m1Min,m1Max])
#plt.ylim([m1Min**2,m1Max**2])
plt.xlim([0.0,0.02])
plt.ylim([0.0,0.001])
#rootName[:10] = "redMoments"
plt.savefig(rootName[:-4]+'-Zoom.png')


    

