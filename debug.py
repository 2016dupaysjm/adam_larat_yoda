#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 17:57:53 2018

@author: larat
"""
import pylab as plt; import numpy as np
X = m.X; getSol = m.param.initFunction.GetExactSolution; Udt = getSol(m,dt); Udemi = getSol(m,0.5*dt)
UL1Dt = np.mean(m.UBar(Udt),axis=0);UL1Demi = np.mean(m.UBar(Udemi),axis=0);
#Err0 = m.UBar(np.abs(U0-Udt))/UBarDt; Err1 = m.UBar(np.abs(U1-Udt))/UBarDt;Err00 = m.UBar(np.abs(U00-Udemi))/UBarDemi;Err10 = m.UBar(np.abs(U10-Udemi))/UBarDemi;Err11 = m.UBar(np.abs(U11-Udemi))/UBarDemi; Err000 = m.UBar(np.abs(U000-Udt))/UBarDt;Err100 = m.UBar(np.abs(U100-Udt))/UBarDt;Err110 = m.UBar(np.abs(U110-Udt))/UBarDt; Err111 = m.UBar(np.abs(U111-Udt))/UBarDt;
Err0 = m.UBar(np.abs(U0-Udt))/UL1Dt; Err1 = m.UBar(np.abs(U1-Udt))/UL1Dt;Err00 = m.UBar(np.abs(U00-Udemi))/UL1Demi;Err10 = m.UBar(np.abs(U10-Udemi))/UL1Demi;Err11 = m.UBar(np.abs(U11-Udemi))/UL1Demi; Err000 = m.UBar(np.abs(U000-Udt))/UL1Dt;Err100 = m.UBar(np.abs(U100-Udt))/UL1Dt;Err110 = m.UBar(np.abs(U110-Udt))/UL1Dt; Err111 = m.UBar(np.abs(U111-Udt))/UL1Dt;
nMom = np.shape(Err0)[-1];legende = (); 
for i in np.arange(nMom): legende += ('Err_M'+str(i)+'/||M'+str(i)+'||_1',)
plt.plot(X,Err0);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err0.png');plt.show();plt.plot(X,Err1);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err1.png');plt.show();plt.plot(X,Err00);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err00.png');plt.show();plt.plot(X,Err10);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err10.png');plt.show();plt.plot(X,Err11);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err11.png');plt.show();plt.plot(X,Err000);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err000.png');plt.show();plt.plot(X,Err100);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err100.png');plt.show();plt.plot(X,Err110);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err110.png');plt.show();plt.plot(X,Err111);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err111.png');plt.show();