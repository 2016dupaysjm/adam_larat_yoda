import numpy as np

### Mother Class
class equation:
    def __init__(self,numFlux,param=1.0):
        self.specificInit(param)
        self.numFluxString = numFlux
        if numFlux == "God":
            self.FStar = self.Godunov
        elif numFlux == "LxF":
            self.FStar = self.LaxFriedrichs
        else:
            print "Unknown numFlux keyword: ", numFlux
            print "Abort!"
            exit
        self.nComp = self.GetNComp() ### Number of Components per State
        ### Specific initialization for some equations
        return
    def specificInit(self,param):
        self.zero  = 0.0 ### under zero, void is considered
        return
    def cMax(self,Ul,Ur):
        print "Equation is not defined! Abort!"
        exit
    def flux(self,U):
        print "Equation is not defined! Abort!"
        exit
    def Jac(self,U):
        print "Jacobian is not defined for this equation! Abort!"
        exit
    def Godunov(self,Ul,Ur):
        print "Equation is not defined! Abort!"
        exit
    def removeZeros(self,U):
        return np.where(U==0.0,1.0,U)
    def positivityLimitation(self,U,m):
        UBar   = m.UBar(U)
        Umin   = 0.0 
        Eps    = np.minimum(UBar,Umin)
        divise = UBar-U
        divise = np.where(divise==0.0,1.0,divise)
        theta = np.where(U<Umin, 
                         0.999*(UBar-Eps)/divise,
                         1.0)
        theta = theta.min(axis=0)
        U     = UBar + theta*(U-UBar) 
        return U
    def LaxFriedrichs(self,Ul,Ur):
        return 0.5*(self.flux(Ul)+self.flux(Ur)-self.cMax(Ul,Ur)*(Ur-Ul))
    def GetNComp(self):
        return 0
    def name(self):
        return ""
        
""" ********** Linear Advection ***********"""
class linAdv(equation):
    def specificInit(self,param):
        self.aaa   = np.array(param)
        return
    def cMax(self,Ul,Ur):
        return self.aaa*np.ones(np.shape(Ul))
    def flux(self,U):
        return self.aaa*U 
    def Jac(self,U):
        Jac    = np.zeros(np.shape(U[...,np.newaxis]))
        Jac[:] = np.array([self.aaa]).reshape((1,1))
        return  Jac
#        shapeU   = np.shape(U)
#        shapeJac = np.append(shapeU,shapeU[-1])
#        if (shapeU[-1] != self.GetNComp()):
#            print "Big Problem. Number of Components should be: ", self.GetNComp()
#            print "when it is: ", shapeU[-1], " Abort!"
#            exit
#        Jac    = np.zeros(shapeJac)
    def Godunov(self,Ul,Ur): 
        if self.aaa>=0: 
            return self.aaa*Ul
        else: 
            return self.aaa*Ur
    def GetNComp(self):
        return 1
    def name(self):
        return "linAdv"
        
""" *************** Burgers ****************"""
class burgers(equation):
    def cMax(self,Ul,Ur):
        return 0.5*np.abs(Ul+Ur)
    def flux(self,U):
        return 0.5*U*U 
    def Jac(self,U):
        return U[...,np.newaxis]
    def Godunov(self,Ul,Ur): 
        return np.where(Ul+Ur>0.0,0.5*Ul**2,0.5*Ur**2)
    def GetNComp(self):
        return 1
    def name(self):
        return "burgers"
               
""" ********** Vectorial Multiple Advection ***********"""
class vecAdv(linAdv):
    def cMax(self,Ul,Ur):
        return np.abs(self.aaa)*np.ones(np.shape(Ul))
    def flux(self,U):
        return self.aaa*U 
    def Jac(self,U):
        shapeU   = np.shape(U)
        shapeJac = np.append(shapeU,shapeU[-1])
        if (shapeU[-1] != self.GetNComp()):
            print "Big Problem. Number of Components should be: ", self.GetNComp()
            print "when it is: ", shapeU[-1], " Abort!"
            exit
        Jac    = np.zeros(shapeJac)
        Jac[:] = np.diag(self.aaa)
        return  Jac
    def Godunov(self,Ul,Ur): 
        return (self.aaa>0.0)*self.aaa*Ul+(self.aaa<0)*self.aaa*Ur
    def GetNComp(self):
        return np.size(self.aaa)
    def name(self):
        return "vecAdv"   
        
""" ********** Multiple Moments Advection ***********"""
class momentsAdv(vecAdv):
    def LaxFriedrichs(self,Ul,Ur):
        return Ul
    def specificInit(self,param):
        self.zero = 1.0e-13
        self.aaa   = np.array(param)
        return
    def verifyAndCorrect(self,U,seuil=-1.0):
        if(seuil<0.0):
            seuil = 1.0e-7
        dzeta = self.computeDzeta(U,0.0)
        dzetaNew = self.nullifyDzeta(dzeta,seuil)
        dzetaNew = np.where(dzetaNew<=0.0,seuil,dzetaNew)
        mom   = self.computeMomentsFromDzeta(dzetaNew)
        return mom
    def nullifyDzeta(self,dzeta,seuil=-1.0):
        if(seuil<0.0):
            seuil = 0.0
        limit = (dzeta<=seuil)
        n     = np.shape(limit)[-1]
        for i in np.arange(n-1)+1:
            limit[...,i] += limit[...,i-1]
        limit = np.where(limit>1,1,limit)
        dzeta *= (1.0-limit)
        return dzeta
    def positivityLimitation(self,U,m):
        UOrig = np.copy(U)
        dzetaOrig = self.computeDzeta(UOrig)
        UBarOrig = m.UBar(UOrig)
        dzetaBarOrig = self.computeDzeta(UBarOrig)
        ### compute Dzetas
        dzeta = self.computeDzeta(U) #,self.zero)
        ### if every quad point is realizable, do nothing.
        if(np.min(dzeta)>=0.0):
            return U
        ### else, compute mean states and limit
        UBar  = m.UBar(U)
        dzetaBar = self.computeDzeta(UBar) #self.zero)
        ### First limit absurd values of UBar. This procedure destroys conservation 
        ###  but only at a level of machine accuracy. 
        #UBar = self.computeMomentsFromDzeta(dzetaBar)
#        U = self.computeMomentsFromDzeta(self.computeDzeta(U,1.0e-16))
#        UBarNew = m.UBar(U)
#        dzetaBarNew = self.computeDzeta(UBarNew)
        if(np.min(dzetaBar)<0.0):
            """ Some values of UBar are too close to the boundaries of the realizable space. 
                The algorithm is very sensible to machine accuracy! 
                Then we add as much machine zero as possible to get off the bad region """
            UBarNew     = np.copy(UBar)
            UNew        = np.copy(U)
            nMom        = np.shape(UBar)[-1]
            nCells      = np.shape(UBar)[0]
            if(np.min(UBarNew[...,0])<0.0):
                print "Wo putain, on a un rho_bar negatif!!!"
                rhoNeg = np.extract(UBarNew[...,0]<0.0,np.arange(nCells))
                print "@Cells: ",rhoNeg
                print "Values: ",UBarNew[rhoNeg,0]
                exit
            prodDzetaBar = np.ones(nCells)
            dzetaBarNew  = self.computeDzeta(UBarNew)
            seuil        = 1.0e-13
            for mom in np.arange(nMom):
###                UBarNew[isLimited,mom]  += (UBarNew[isLimited,mom]*seuil + (seuil-dzetaBarNew[isLimited,mom])*prodDzetaBar)*(dzetaBarNew[isLimited,mom]<0.0)
                UNew[...,mom]  += (seuil-dzetaBarNew[...,mom]*prodDzetaBar)*((prodDzetaBar*dzetaBarNew[...,mom])<seuil) 
                dzetaBarNew     = self.computeDzeta(m.UBar(UNew))
                prodDzetaBar    *= dzetaBarNew[...,mom]
            if(np.min(dzetaBarNew)<0.0): 
                print "OUla Caca!!!"
                exit
            U = UNew
# ########## Limit All moments at once
#        U = self.limitAllAtOnce(U,UBar)
#        return U
# ########## Limit reduced-moments All at once
        if(np.min(dzeta[...,0])<0.0):
            U = self.limitRho(U,UBar,m)
        U = self.limitAutosimilar(U,UBar)
        return U
# ########## Limit reduced-moments One by one
#        UOne        = np.copy(U)
#        UOne[...,0] = np.where(UOne[...,0]==0.0,-1.0,UOne[...,0])
#        UOne        = np.where((UOne[...,0]>0.0)[...,np.newaxis],UOne/UOne[...,0,np.newaxis],0.0)
#        UOne[...,0] = np.where(UOne[...,0]==-1.0,0.0,UOne[...,0])
#  ###      #UBarOne     = UBar/UBar[...,0,np.newaxis]
#        UBarOne        = np.copy(UBar)
#        UBarOne[...,0] = np.where(UBarOne[...,0]==0.0,-1.0,UBarOne[...,0])
#        UBarOne       /= UBarOne[...,0,np.newaxis]
#        UBarOne[...,0] = np.where(UBarOne[...,0]==-1.0,0.0,UBarOne[...,0])
#        
#        N = np.shape(U)[-1]
#        for i in np.arange(N-1)+1:
#            UOne = self.limitMin(i,UOne,UBarOne)
#        ### Come back to full moments
#        return U[...,0,np.newaxis]*UOne
    def limitRho(self,U,UBar,m):
        ### 1) On annule ce qu'on doit annuler: 
        """   Option a) rho<0 => xxx% de rho_Bar (on limite a un certain pourcentage de la valeur moyenne) """
#        U[...,0] = np.where((U[...,0]<=0.0),0.01*UBar[...,0],U[...,0])
        """   Option b) rho<0 => rho=0.0 => tout le vecteur de moment devient 0.0 """
        zeroVect = np.zeros(np.shape(U)[-1])
        U = np.where((U[...,0,np.newaxis]<=0.0),zeroVect,U)
        ### 2) Limit
        UBarNew = m.UBar(U)
        UBarNew = np.where(UBarNew==0.0,1.0,UBarNew)
        U *= UBar/UBarNew
        return U
    def limitAllAtOnce(self,U,UBar):
        ### In this limitation, U is limited in the Zhang & Shu manner, simply toward UBar, dof per dof at 
        ### the maximal theta for which ALL the dzetas become strictly positive. 
        dzetaTilde  = self.computeDzeta(U)
        doLimit     = (np.min(dzetaTilde,axis=(0,-1),keepdims=True)<0.0) #+ (np.min(U,axis=(0,-1),keepdims=True)<=0.0)
        deltaU = U-UBar
        thetaR = np.ones(np.shape(U[...,0,np.newaxis]))
        thetaL = thetaR*0.0
        nDico = 30
        for j in np.arange(nDico): 
            theta = 0.5*(thetaL+thetaR)
            UTilde = UBar + theta*deltaU
            dzetaTildeNew = self.computeDzeta(UTilde)
            dzetaTildeMin = np.min(dzetaTildeNew,axis=-1,keepdims=True)
            thetaL   = np.where(dzetaTildeMin>0.0,theta,thetaL)
            thetaR   = np.where(dzetaTildeMin<=0.0,theta,thetaR)
        thetaMin = np.min(thetaL,axis=0)
        theta    = np.where(doLimit,thetaMin,1.0)
        ### Limit U with theta
        U = UBar + theta*deltaU
        return U
    def limitAutosimilar(self,U,UBar):
        ### In this limitation, U[...,0] should be non-negative everywhere, it has been limited by limitRho(...)
        ### The moment space is self-similar in rho and the limitation is then performed in the (1,m_1/m_0,...,m_k/m_0) reduced space.
        ### It is performed the same way as Zhang & Shu
        #UOld   = np.copy(U)
        UOne        = np.copy(U)
        UOne[...,0] = np.where(UOne[...,0]==0.0,-1.0,UOne[...,0])
        UOne        = np.where((UOne[...,0]>0.0)[...,np.newaxis],UOne/UOne[...,0,np.newaxis],0.0)
        UOne[...,0] = np.where(UOne[...,0]==-1.0,0.0,UOne[...,0])
        dzetaTilde  = self.computeDzeta(UOne)
        doLimit     = (np.min(dzetaTilde,axis=(0,-1),keepdims=True)<0.0) #+ (np.min(U,axis=(0,-1),keepdims=True)<=0.0)
        #UBarOne     = UBar/UBar[...,0,np.newaxis]
        UBarOne        = np.copy(UBar)
        UBarOne[...,0] = np.where(UBarOne[...,0]==0.0,-1.0,UBarOne[...,0])
        UBarOne       /= UBarOne[...,0,np.newaxis]
        UBarOne[...,0] = np.where(UBarOne[...,0]==-1.0,0.0,UBarOne[...,0])
        deltaU = UOne-UBarOne
        thetaR = np.ones(np.shape(U[...,0,np.newaxis]))
        thetaL = thetaR*0.0
        nDico = 30
        for j in np.arange(nDico): 
            theta = 0.5*(thetaL+thetaR)
            UTilde = UBarOne + theta*deltaU
            dzetaTildeNew = self.computeDzeta(UTilde)
            dzetaTildeMin = np.min(dzetaTildeNew,axis=-1,keepdims=True)
            thetaL   = np.where(dzetaTildeMin>0.0,theta,thetaL)
            thetaR   = np.where(dzetaTildeMin<=0.0,theta,thetaR)
        thetaMin = np.min(thetaL,axis=0)
        theta    = np.where(doLimit,thetaMin,1.0)
        ### Limit U[...,i:] with theta
        U = U[...,0,np.newaxis]*(UBarOne + theta*deltaU)
#        dzeta = self.computeDzeta(U)
#        if(np.min(dzeta)<0.0):
#            """ Some values of U are too close to the boundaries of the realizable space. 
#                The algorithm is very sensible to machine accuracy! 
#                Then we add as much machine zero as possible to get off the bad region """
#            nMom     = np.shape(U)[-1]
#            UNew     = np.copy(U)
#            dzetaNew = np.copy(dzeta)
#            for i in np.arange(nMom):
#                #UNew     += self.zero*(dzetaNew<0.0)
#                UNew     *= (1.0+1.0e-15*(dzetaNew<0.0))
#                dzetaNew  = self.computeDzeta(UNew,self.zero)
#                if(np.min(dzetaNew)>=0.0):
#                    break
#                if(i==nMom-1):
#                    print "M_Tilde is not realizable. Abort !"
#                    print "***************** dzeta ************"
#                    print dzeta
#                    print "***************** dzetaNew ************"
#                    print dzetaNew
#                    print "***************** dzetaBar = self.computeDzeta(UBar) ************"
#                    print self.computeDzeta(UBar)
#                    print "M_Tilde is not realizable. Abort !"
#                    print "i = ",i
#                    exit
        return U
    def limitMin(self,i,U,UBar): ### 2018.04.11 Ne fonctionne pas. Toujours pire que LimitAutosimilar !
        ### Notations: U is the current values at Dofs. May be not-realizable.
        ###            UBar is the mean value per cells. Should be everywhere realizable
        ###            UTilde is the limited version of U, obtained incrementally, moment per moment.
        ###            deltaU = U-UBar
        ### Let's call UBarTilde_{i-1} = concat(UTilde[...,:i],UBar[...,i:])
        ### The vector UBarTilde_{i-1} with first (i-1)-th component identical to UTilde is realizable by construction 
        ### (if i==0, UBarTilde = UBar)
        ### Then the limiting algorithm reads as follows: 
        ### 1) Construct UBarTilde_{i}
        UBarTilde           = np.copy(U)
        UBarTilde[...,i+1:] = UBar[...,i+1:]
        dzetaTildeNew       = self.computeDzeta(UBarTilde)
        deltaU              = U-UBar
        ### 2) If UBarTilde_{i} is realizable, return. 
        ###    Else, since UBarTilde_{i-1} is realizable, there must exist theta\in[0.0,1.0], such that 
        ###    concat(UTilde[...,:i],UBar[...,i]+theta*deltaU[...,i],UBar[...,i+1:]) is realizable. 
        ###    Find it by nDico steps of dichotomy
        if(np.min(dzetaTildeNew)>=0.0):
            return U
        thetaR = np.ones(np.shape(U)[:-1])
        #thetaL = thetaR*0.0
        dzetaTildeMin = np.min(dzetaTildeNew,axis=-1)
        thetaL = np.where(dzetaTildeMin>=0.0,1.0,0.0)
        nDico = 30
        for j in np.arange(nDico): 
            theta = 0.5*(thetaL+thetaR)
            UBarTilde[...,i] = UBar[...,i] + theta*deltaU[...,i]
            dzetaTildeNew = self.computeDzeta(UBarTilde)
            dzetaTildeMin = np.min(dzetaTildeNew,axis=-1)
            thetaL   = np.where(dzetaTildeMin>0.0,theta,thetaL)
            thetaR   = np.where(dzetaTildeMin<=0.0,theta,thetaR)
        thetaMin = np.min(thetaL,axis=0)
        ### 5) Limit U[...,i] with theta
        U[...,i] = UBar[...,i] + thetaMin*deltaU[...,i]
        return U
    def limit(self,i,U,UBar):
        ### Notations: U is the current values at Dofs. May be not-realizable.
        ###            UBar is the mean value per cells. Should be everywhere realizable
        ###            UTilde is the limited version of U, obtained incrementally, moment per moment.
        ###            deltaU = U-UBar
        ### The vector UBarTilde_{i-1} with first (i-1)-th component identical to UTilde is realizable by construction (if i==0, UBarTilde = UBar)
        ### Let's call it UBarTilde_{i-1} = concat(UTilde[...,:i],UBar[...,i:])
        ### 1) First find theta such that concat(UTilde[...,i],UBar[...,i]+theta*deltaU[...,i]) is realizable. 
        ###    Since U[...,:i]=UTilde[...,:i] is fixed, this should be a linear problem in theta.        
        dzeta = self.computeDzeta(U)
        if(np.min(dzeta[...,i])>=0.0): 
            return U
        ###    Construct UBarTilde_{i-1} which is realizable and limit with respect to it.
        limit = dzeta<0.0
        N     = np.shape(U)[-1]
        for j in np.arange(N-i-1)+i+1:
            limit[...,j]+=limit[...,j-1]
        #UBarTilde = np.copy(U)
        UBarTilde = np.where(limit,UBar,U)
        dzetaBarTilde = self.computeDzeta(UBarTilde)
        if(np.min(dzetaBarTilde[...,i])<0.0):
            print dzetaBarTilde
            print limit
            print "Exiting"
            exit
        deltaDzeta = dzeta-dzetaBarTilde
        #UOld = np.copy(U)
        deltaU = U-UBar
        thetaR = (self.zero-dzetaBarTilde[...,i])/np.where(deltaDzeta[...,i]==0,self.zero-dzetaBarTilde[...,i],deltaDzeta[...,i])
        thetaR = np.where(thetaR>self.zero,thetaR-self.zero,thetaR) ### In certain case, machine zero is misleading. This should fix it...
        thetaR = np.where((thetaR<0.0)+(thetaR>1.0),1.0,thetaR)
        thetaMin = np.min(thetaR,axis=0)
        ### 2) Limit the 'i:' resting components by thetaR and verify that the vector of moments is realizable.
        U[...,i:] = UBar[...,i:] + thetaMin[...,np.newaxis]*deltaU[...,i:]
        dzetaNew = self.computeDzeta(U)
        return U
        if(np.min(dzetaNew[...,:i+1])<0.0):
            print "UTilde not in S"
            print "Abort!"
            exit
        if(np.min(dzetaNew)>=0.0):
            return U
        ### 3) Construct UBarTilde_{i}
        UBarTilde = np.copy(U)
        UBarTilde[...,i+1:] = UBar[...,i+1:]
        dzetaTildeNew = self.computeDzeta(UBarTilde)
        ### 4) If UBarTilde_{i} is realizable, return. Else, since UBarTilde_{i-1} is realizable, there must exist theta\in[0,thetaR], such that 
        ###    concat(UTilde[...,:i],UBar[...,i]+theta*deltaU[...,i],UBar[...,i+1:]) is realizable. Find it by nDico steps of dichotomy
        if(np.min(dzetaTildeNew)>=0.0):
            return U
        thetaL = thetaR*0.0
        nDico = 30
        for j in np.arange(nDico): 
            theta = 0.5*(thetaL+thetaR)
            UBarTilde[...,i] = UBar[...,i] + theta*deltaU[...,i]
            dzetaTildeNew = self.computeDzeta(UBarTilde)
            dzetaTildeMin = np.min(dzetaTildeNew,axis=-1)
            thetaL   = np.where(dzetaTildeMin>0.0,theta,thetaL)
            thetaR   = np.where(dzetaTildeMin<=0.0,theta,thetaR)
        thetaMin = np.min(thetaL,axis=0)
        ### 5) Limit U[...,i:] with theta
        U[...,i:] = UBar[...,i:] + thetaMin[...,np.newaxis]*deltaU[...,i:]
        return U
    def checkRealizability(self,U,m):
        ### First check m_0 are positive
        """   
        if(sum(U[...,0]<0)>0):
            print "moment 0 is negative somewhere"
            print U[...,0]
            return False
        """
#        s = np.shape(U)
#        rootShape = s[:-1]
#        N         = s[-1]
#        assert(N == self.GetNComp())
#        halfN  = N//2
#        if N==1: ### One Moment Only
#            return True
#        elif(halfN==0): ### m_0 and m_1
#            if(sum(U[...,1]<0)>0):
#                print "moment 1 is negative somewhere"
#                print U[...,1]
#                return False
#            else:
#                return True
        dzeta = self.computeDzeta(U)
        if(np.min(dzeta)<0):
            print dzeta
            return False
        return True
    def computeDzeta(self,U,zero=-1.0):        
        # zero is the magnitude under which dzetas are considered to be null. 
        # this is useful to eliminate numerical instabilities due to machine accuracy.
        if(zero<0.0) : 
            zero = 0.0 #self.zero
        s = np.shape(U)
        rootShape = s[:-1]
        N         = s[-1]
        halfN     = (N+1)//2
        dzeta = np.zeros(s)
        Z     = np.zeros(np.append(rootShape,(N,halfN+1)))
        alpha = np.zeros(np.append(rootShape,halfN+1))  
        beta  = np.zeros(np.append(rootShape,halfN+1))  
        ### Convention, dzeta(0) = m_0
        dzeta[...,0] = U[...,0]
        if(N==1):
            ### Put Zeros when at the realizability limits
            return np.where(np.abs(dzeta)<=zero,0.0,dzeta)
        ### Z(-1,k) = 0.0 and Z(0,k) = m_k
        mass     = np.where(U[...,0]==0.0,1.0,U[...,0])
        Z[...,0] = U/mass[...,np.newaxis]
        alpha[...,0] = U[...,1]/mass
        dzeta[...,1] = np.where(np.abs(U[...,0])<=zero,0.0,alpha[...,0])
        if(N==2): 
            ### Put Zeros when at the realizability limits
            return np.where(np.abs(dzeta)<=zero,0.0,dzeta)
        Z[...,1]     = np.roll(Z[...,0],shift=-1,axis=-1)-alpha[...,np.newaxis,0]*Z[...,0]
        diag0        = np.where(Z[...,0,0]==0.0,1.0,Z[...,0,0])
        diag1        = np.where(Z[...,1,1]==0.0,1.0,Z[...,1,1])
        beta [...,1] = Z[...,1,1]/diag0
        alpha[...,1] = Z[...,2,1]/diag1-Z[...,1,0]/diag0
        dzetaNonNul  = np.where(dzeta[...,1]==0.0,1.0,dzeta[...,1])
        dzeta[...,2] = beta[...,1]/dzetaNonNul
        dzeta[...,2] = np.where(np.abs(dzeta[...,1])<=zero,0.0,dzeta[...,2])
        if N==3: 
            ### Put Zeros when at the realizability limits
            return np.where(np.abs(dzeta)<=zero,0.0,dzeta)
        dzeta[...,3] = np.where(np.abs(dzeta[...,2])<=zero,0.0,alpha[...,1]-dzeta[...,2])
        for i in np.arange(halfN-2)+2:
            Z[...,i]     = np.roll(Z[...,i-1],shift=-1,axis=-1)-alpha[...,np.newaxis,i-1]*Z[...,i-1]-beta[...,np.newaxis,i-1]*Z[...,i-2]
            diagIm = np.where(Z[...,i-1,i-1]==0.0,1.0,Z[...,i-1,i-1])            
            diagI  = np.where(Z[...,i,i]    ==0.0,1.0,Z[...,i,i]) 
            beta[...,i]  = Z[...,i,i]/diagIm
            alpha[...,i] = Z[...,i+1,i]/diagI-Z[...,i,i-1]/diagIm
            dzetaNonNul  = np.where(dzeta[...,2*i-1]==0.0,1.0,dzeta[...,2*i-1])
            dzeta[...,2*i  ] = beta [...,i]/dzetaNonNul
            dzeta[...,2*i  ] = np.where(np.abs(dzeta[...,2*i-1])<=zero,0.0,dzeta[...,2*i])
            if (2*i+1)<N:
                dzeta[...,2*i+1] = np.where(np.abs(dzeta[...,2*i])<=zero,0.0,alpha[...,i]-dzeta[...,2*i])
        ### Put Zeros when at the realizability limits
        dzeta = np.where(np.abs(dzeta)<=zero,0.0,dzeta)
#        for i in np.arange(N-1)+1:
#            dzeta[...,i] = np.where(np.abs(dzeta[...,i-1])>self.zero,dzeta[...,i],0.0)
        return dzeta
    def computeMomentsFromDzeta(self,dzeta):
        s         = np.shape(dzeta)
        rootShape = s[:-1]
        N         = s[-1]        
        halfN     = (N+1)//2
        Z         = np.zeros(np.append(rootShape,(halfN+1,N)))
        alpha     = np.zeros(np.append(rootShape,halfN))
        beta      = np.zeros(np.append(rootShape,halfN))
        ### 1) Compute alphas and betas from dzetas
        I2  = np.arange(halfN)*2
        alpha        = dzeta[...,I2]+dzeta[...,(I2+1)*(I2+1<N)]
        beta[...,1:] = dzeta[...,I2[1:]]*dzeta[...,I2[1:]-1]
        alpha[...,0] = dzeta[...,1]
        beta[...,0]  = 0.0
        ### 2) Initialize Z with m_0
        Z[...,1,0] = dzeta[...,0]
        ### 3) Then formula Z_{k,k+i} = Z_{k+1,k+i-1}+alpha_k*Z_{k,k+i-1}+beta*Z_{k-1,k+i-1} is enough
        ###      to finish the computation
        index = np.arange(halfN+1)
        I     = index[1:]
        Ip    = np.roll(index,-1)[1:]
        Im    = index[:-1]
        for i in np.arange(N-1)+1:
            Z[...,I,i] = Z[...,Ip,i-1]+alpha*Z[...,I,i-1]+beta*Z[...,Im,i-1]
        ### Sought moments are simply Z_{0,k} which corresponds here to line 1, since line 0 is 
        ###   Z_{-1,k} = 0
        return Z[...,1,:]
    def name(self):
        return "momentsAdv"   
            
""" ********** Pressure-Less Gas Dynamics ***********"""
class PGD(equation):
    def specificInit(self,param):
        self.zero = 1.0e-13
        return
    def getVelocity(self,U):
        Ut = U.transpose()
        return np.where(Ut[0]==0.0,
                        0.0,
                        Ut[1]/self.removeZeros(Ut[0])
                       ).transpose()
    def cMax(self,Ul,Ur):
        return np.maximum(np.abs(self.getVelocity(Ul)),
                          np.abs(self.getVelocity(Ur)))[...,np.newaxis]     
    def flux(self,U):
        u = self.getVelocity(U)
        ### add an extra dimension to u for the next array multiply
        u = np.expand_dims(u,np.size(np.shape(u)))
        return U*u
    def limitRho(self,U,m):
        RhoMin = self.zero
        Rho  = U[:,:,0]
        ### Mean Values
        UBar   = m.UBar(U)
        RhoBar = UBar[:,0]
        ### Limit Rho
        EpsRho = np.minimum(RhoBar,RhoMin)
        divise = RhoBar-Rho
        divise = np.where(divise==0.0,1.0,divise)
        theta  = np.where(Rho<RhoMin, 
                          (RhoBar-EpsRho)/divise,
                          1.0)
        theta    = theta.min(axis=0)
        U[:,:,0] = RhoBar  + theta*(Rho-RhoBar)
        return U
    def limitVelocity(self,U,m):
        ### Limit velocity for maximum principle
        vMin  = -1.0+self.zero; vMax = 1.0-self.zero
        v     = self.getVelocity(U)
        ### Mean Values
        UBar  = m.UBar(U)
        vBar  = self.getVelocity(UBar)
        ### Velocity Boundaries
        EpsUMin = np.minimum(vMin,vBar)
        EpsUMax = np.maximum(vMax,vBar)
        ### Limit Velocity
        divise = vBar-v
        divise = np.where(divise==0.0,1.0,divise)
        thetaMin = np.where(v<vMin,(vBar-EpsUMin)/divise,1.0)
        thetaMax = np.where(v>vMax,(vBar-EpsUMax)/divise,1.0)
        theta    = np.minimum(thetaMin,thetaMax).min(axis=0)
        U[:,:,1] = U[:,:,0]*(vBar+theta*(v-vBar))
        return U
    def positivityLimitation(self,U,m):
        U = self.limitRho(U,m)
        U = self.limitVelocity(U,m)
        return U
    def Godunov(self,Ul,Ur): 
        print "Godunov not implemented yet for PGD"
        print "Abort!"
        exit
    def GetNComp(self):
        return 2
    def name(self):
        return "PGD"
        
""" ********** Euler ***********"""
class Euler(PGD):
    def specificInit(self,param):
        self.zero = 1.0e-13
        self.gamma = param
        return
    ### Given by PGD : def getVelocity(self,U):
    def getPressure(self,U):
        Ut = U.transpose()
        p  = (self.gamma-1.0)*(Ut[2]-0.5*Ut[1]*self.getVelocity(U).transpose())   
        p  = np.where((p<0.0)*(np.abs(p)<self.zero),0.0,p)         
        return p.transpose()
    def getSoundVelocity(self,U):
        Ut = U.transpose()
        Rho = self.removeZeros(Ut[0])
        p   = self.getPressure(U).transpose()
        if(np.min(p)<0.0):
            print "P<0", np.min(p)
        return np.where(Ut[0]<self.zero,0.0,np.sqrt(self.gamma*p/Rho)).transpose()
    def cMax(self,Ul,Ur):
        return np.maximum(np.abs(self.getVelocity(Ul))+self.getSoundVelocity(Ul),
                   np.abs(self.getVelocity(Ur))+self.getSoundVelocity(Ur))[...,np.newaxis]  
    def flux(self,U):
        Ut = U.transpose()
        f = np.zeros(np.shape(Ut))       
        f[0] = Ut[1]
        f[1] = Ut[1]*self.getVelocity(U).transpose()+self.getPressure(U).transpose()
        f[2] = (Ut[2]+self.getPressure(U).transpose())*(self.getVelocity(U)).transpose()
        return f.transpose()
    def limitPressure(self,U,m):
        pMin  = self.zero;
        p     = self.getPressure(U)
        UBar  = m.UBar(U)
        pBar  = self.getPressure(UBar)
        epsP  = np.minimum(pBar,pMin)
        divise = pBar-p
        divise = np.where(divise==0.0,1.0,divise)
        theta  = np.where(p<pMin,(pBar-epsP)/divise,1.0)
        theta  = theta.min(axis=0)
        pTilde = pBar+theta*(p-pBar)
        U[:,:,2] = pTilde/(self.gamma-1.0)+0.5*U[:,:,0]*self.getVelocity(U)**2
        return U
    def positivityLimitation(self,U,m):
        U = self.limitRho(U,m)
        U = self.limitVelocity(U,m)
        U = self.limitPressure(U,m)
        return U
    def Godunov(self,Ul,Ur): 
        print "Godunov not implemented yet for Euler"
        print "Abort!"
        exit
    def GetNComp(self):
        return 3
    def name(self):
        return "Euler"