import miscRoutines1D as mR
import mesh1D as mE1D
import numpy as np
import pdb

half = 0 # for the sake of consistency with the math notations
nbDigit = 3  # nb of digits for numbering the output files name 
ng = 2  # nb of ghost cells on each side of the domain

pdb.set_trace()

print("Compute: begin")
xMin = 0.0
xMax = 1.0
nx = 100    


#  0   | ... |ng-1 || ng+ | ng+ | ng+ |                             |ng + || ng+ | ... | ng+
#      |     |     ||  0  |  1  |  2  |                             |nx-1 || nx  |     | nx+ng-1  
# --x--|--x--|--x--||--x--|--x--|--x--|--x--|--x--|--x--|--x--|--x--|--x--||--x--|--x--|--x--
#      |     |     ||     |     |     |     |     |     |     |     |     ||     |     |      
#      ^     ^     ^      ^     ^     ^     ^     ^     ^     ^     ^     ^      ^     ^      
#      0          ng-1    ng                                             ng +         ng+
#                                                                        nx-1         nx+ng-2

# create the mesh = discretization of [xMin, xMax] into nx cells (using ng ghost cells)
m = mE1D.Mesh1D(nx, ng, xMin, xMax)

# get the x_j from the mesh
x = m.GetCoord()

# create a cell-centered array of data (with nb ghost cells) 
c = m.NewCellArray()

# retrieve indices for all cells (including ghost)
I = m.GetAllCellId()

# set data for c by applying : c_i = cos(2*pi*x_k)
c[I] = np.cos(2*np.pi*x[I])

# save the data x_i,c_i "gnuplot style" (ghost values are not saved)
fileName = "newVarFile.dat"
mR.SaveData(fileName, c[I], x[I])

# create an edge-centered array of data
ec = m.NewEdgeArray()

# retrieve indices for all edges (including ghost)
I = m.GetAllEdgeId()

# compute some formula
ec[I + half] = 0.5 * (c[I] + c[I + 1])

# applying periodic BC by performing
# c_k = c_k + nx,            for  k=0,...,ng-1
# c_k + nx + ng= c_k + ng,   for k=0,...,ng-1
mE1D.PeriodicBC(m, c)

# looping over all the inner edges (ghost edges are excluded)
I = m.GetInnerEdgeId()
for i in I:
    print("ec["+str(i)+"]="+str(ec[i]))

# looping over all the inner cells (ghost edges are excluded)
I = m.GetInnerCellId()
for i in I:
    print("c["+str(i)+"]="+str(c[i]))
    
# taking the maximum of an array
I = m.GetInnerCellId()
cMax = np.max(c[I])
print("cMax = "+str(cMax))

# compute the array m_i = min(x_i, cos(x_i))
I = m.GetInnerCellId()
m = np.minimum(x[I],np.cos(x[I]))
