import numpy as np
import math
import equations
from RiemannEuler import RiemannEuler

### Mother Class 
class initFunction:
    def __init__(self,p,eqParam=1.0):
        self.zero  = 1.0e-13 ### Needed for positivity limitation
        self.nDofs = 1       ### Number of DoFs per Cells
        self.nComp = self.GetNComp() ### Number of Components per DoFs
        self.param = p       ### Get Computation parameters. Needed to call the right equation
        self.specificInit(eqParam)
        return
    def specificInit(self,eqParam):
        print "Specific Init not implemented in this case: ",self.name()
        exit
    def initialize(self,m):
        self.m = m
        if m.GetNComp() != self.nComp:
            print "Number of components in the equation is not the same as in the initial condition"
            print "Please fix that. The computation is aborting!"
            exit
        x  = m.GetCoords()
        Xq = m.quads.GetCoords()
        xCoord = np.outer(np.ones(np.size(Xq)),x)+0.5*m.dx*Xq[:,np.newaxis]
        return self.init(xCoord)
    def GetExactSolution(self,m,t):
        x  = m.GetCoords()
        Xq = m.quads.GetCoords()
        xCoord = np.outer(np.ones(np.size(Xq)),x)+0.5*m.dx*Xq[:,np.newaxis]
        UExact = self.ExactSolution(xCoord,t)
        return UExact
#        
#        UExact  = m.NewCellsArray(self.nDofs,self.nComp)
#        for i in np.arange(self.nDofs):
#            UExact[i] = self.ExactSolution(x+0.5*m.dx*Xq[i],t)
#        return UExact
    def ExactSolution(self,x,t):
        print "Exact solution is not defined is this case: ", self.name()
        print "Please provide the solution accordingly!"
        exit
    def GetNComp(self):
        return 0
    def getEquation(self):
        return self.equation
    def name():
        return "initFunction"

### Linear Advection initFunctions
class linearAdvection(initFunction):
    def specificInit(self,eqParam=1.0):
        self.equation = equations.linAdv(self.param.numFlux)
        return
    def GetNComp(self):
        return 1
    def name():
        return "linearAdvection"
    def ExactSolution(self,x,t):
        xFinal = x-self.equation.aaa*t
        return self.init(xFinal-np.floor(xFinal))
        
### Burgers initFunctions
class burgersInit(initFunction):
    def specificInit(self,eqParam=1.0):
        self.equation = equations.burgers(self.param.numFlux)
        return
    def GetNComp(self):
        return 1
    def name():
        return "burgersInit"
    def ExactSolution(self,x,t):
        print "There is no generic Exact solution for Burgers' problems"
        exit
        
### Vectorial Multiple Advection 
class vectorialAdvection(initFunction):
    def __init__(self,p,initCondVector,velocities):
        self.zero  = 1.0e-13 ### Needed for positivity limitation
        self.param = p       ### Get Computation parameters. Needed to call the right equation
        self.nDofs = 1       ### Number of DoFs per Cells
        if((np.size(initCondVector) != np.size(velocities)) & (initCondVector[0][:7] != "moments")):
            print "Size Mismatch in vectorial Advection Initialization"
            print np.size(initCondVector), "!=", np.size(velocities)
            print "Abort!"
            #exit
        self.nComp = np.size(velocities) ### Number of Components per DoFs
        self.initVector = initCondVector
        self.velocities = velocities
        self.equation   = equations.vecAdv(self.param.numFlux,self.velocities)
        return  
    def init(self,x):
        U = np.zeros(np.append(np.shape(x),self.nComp))
        for i in np.arange(self.nComp):
            if self.initVector[i] == "cos4x":
                U[...,i] = cos4x(self.param).init(x)[...,0]
            elif self.initVector[i] == "gaussian":
                U[...,i] = gaussian(self.param).init(x)[...,0]
            elif self.initVector[i] == "hat":
                U[...,i] = hat(self.param).init(x)[...,0]
            elif self.initVector[i] == "periodicCosine":
                U[...,i] = periodicCosine(self.param).init(x)[...,0]
            elif self.initVector[i] == "localizedCosine":
                U[...,i] = localizedCosine(self.param).init(x)[...,0]
            elif self.initVector[i] == "mixed":
                U[...,i] = mixed(self.param).init(x)[...,0]
            else:
                print "Unknown Keyword in initCondVector", self.initVector[i]
                print "Abort!"
                exit
        return U
    def ExactSolution(self,x,t): 
        for i in np.arange(self.nComp):
            U = np.zeros(np.append(np.shape(x),self.nComp))
            xFinal  = x-self.velocities[i]*t
            xFinal -= np.floor(xFinal)
            if self.initVector[i] == "cos4x":
                U[...,i] = cos4x(self.param).init(xFinal)[...,0]
            elif self.initVector[i] == "gaussian":
                U[...,i] = gaussian(self.param).init(xFinal)[...,0]
            elif self.initVector[i] == "hat":
                U[...,i] = hat(self.param).init(xFinal)[...,0]
            elif self.initVector[i] == "periodicCosine":
                U[...,i] = periodicCosine(self.param).init(xFinal)[...,0]
            elif self.initVector[i] == "localizedCosine":
                U[...,i] = localizedCosine(self.param).init(xFinal)[...,0]
            elif self.initVector[i] == "mixed":
                U[...,i] = mixed(self.param).init(xFinal)[...,0]
            else:
                print "Unknown Keyword in initCondVector", self.initVector[i]
                print "Abort!"
                exit
        return U
    def GetNComp(self):
        return self.nComp
    def name(self):
        string = ""
        for i in self.initVector:
            string+= i+"_"
        return string[:-1]  
        
        
### Moments Advection
class momentsAdvection(initFunction):
    def __init__(self,p,initSizeDistribution,N):
        self.zero = 1.0e-13; ### Needed for positivity limitation
        self.param = p       ### Get Computation parameters. Needed to call the right equation
        self.nDofs = 1       ### Number of DoFs per Cells
        self.nComp = N       ### Number of Components per DoFs
        self.velocities  = np.zeros(N)+1.0 ### All moments move at velocity 1.0
        self.equation    = equations.momentsAdv(self.param.numFlux,self.velocities)
        self.sizeDistrib = initSizeDistribution
        return
    def init(self,x):
        if  (self.sizeDistrib == "Beta"):
            U = momentsBeta(self.param).init(x,self.nComp)
        elif self.sizeDistrib == "Gamma":
            U = momentsGamma(self.param).init(x,self.nComp)
        elif self.sizeDistrib == "Border":
            U = momentsBorder(self.param).init(x,self.nComp)
        else:
            print "Unknown Keyword in initCondVector/moments:", self.initVector
            print "Abort!"
            exit
        UNew = self.equation.positivityLimitation(U,self.m)
        print "Initial Function has been limited"
        print "U_0 Mean Values:      ", np.sum(self.m.UBar(U),axis=0)*self.m.dx
        print "Limitation Mass Loss: ", np.sum(self.m.UBar(UNew-U),axis=0)*self.m.dx
        print "********************** OK, The new limited initial solution is taken as the reference **************"
        
        #U = self.equation.verifyAndCorrect(U)#,1.0e-13)
        return UNew
    def ExactSolution(self,x,t):
        xFinal  = x-self.velocities[0]*t
        xFinal -= np.floor(xFinal)
        if self.sizeDistrib == "Beta":
            U = momentsBeta(self.param).init(xFinal,self.nComp)
        elif self.sizeDistrib == "Gamma":
            U = momentsGamma(self.param).init(xFinal,self.nComp)
        elif self.sizeDistrib == "Border":
            U = momentsBorder(self.param).init(xFinal,self.nComp)
        else:
            print "Unknown Keyword in initCondVector/moments", self.initVector
            print "Abort!"
            exit
        return U
    def GetNComp(self):
        return self.nComp
    def name(self):
        return "moments"+self.sizeDistrib       
        
        
### PGD initFunctions
class PGDInit(initFunction):
    def specificInit(self,eqParam=1.0):
        self.equation = equations.PGD(self.param.numFlux)
        return
    def GetNComp(self):
        return 2
    def name():
        return "PGDInit"
    def ExactSolution(self,x,t):
        print "There is no generic Exact solution for PDG problems"
        exit
        
### Euler initFunctions
class EulerInit(initFunction):
    def specificInit(self,gamma):
        self.gamma    = gamma
        self.equation = equations.Euler(self.param.numFlux,gamma)
        return
    def GetNComp(self):
        return 3
    def name():
        return "EulerInit"
    def ExactSolution(self,x,t):
        print "There is no generic Exact solution for Euler problems"
        exit
        
""" **************************************************************"""
""" ******************* linAdv InitFunctions *********************"""
""" **************************************************************"""

class cos4x(linearAdvection):
    def init(self,x):
        return np.cos(4 * x)[...,np.newaxis]
    def name(self):
        return "cos4x"

class gaussian(linearAdvection):
    def init(self,x):
        return np.exp(-(x-0.5)**2/(2*0.01**2))[...,np.newaxis]
    def name(self):
        return "gaussian"

class hat(linearAdvection):
    def init(self,x):
        return ((0.25 < x)*(x < 0.6)*1.0)[...,np.newaxis]
    def name(self):
        return "hat"

class periodicCosine(linearAdvection):
    def init(self,x):
        return (0.5*(np.cos(2*math.pi*x)+1.0))[...,np.newaxis]
    def name(self):
        return "periodicCosine"

class localizedCosine(linearAdvection):
    def init(self,x):
        return ((x>0.25)*(x<0.75)*np.cos(2*np.pi*x)**4)[...,np.newaxis]
    def name(self):
        return "localizedCosine"

class localizedCosine10(linearAdvection):
    def init(self,x):
        return ((x>0.25)*(x<0.75)*np.cos(2*np.pi*x)**10)[...,np.newaxis]
    def name(self):
        return "localizedCosine10"
        
class mixed(linearAdvection):
    def init(self,x):
        return (
            0.5 * (1 + np.sin(4 * math.pi * x - math.pi * .5)) 
            * (x < 0.5) 
            * (0 < x) + (0.6 < x) 
            * (x < 0.85)
            )[...,np.newaxis]  # to be used with (xMin,xMax) = (0,1)
    def name(self):
        return "mixed"
        
""" **************************************************************"""
""" *********** Moments Transport InitFunctions ******************"""
""" **************************************************************"""

def hh(x):
    y      = -2.0*np.tan(np.pi*(x-0.5))*(x>=0.0)*(x<=1.0)
    mask   = np.sign(y)*(np.abs(y)>16*np.log(10))
    y      = np.where(np.abs(mask)>0.1,0.0,y)
    result = (x>0.0)*(1.0/(1.0+np.exp(y))*(x<1.0)+(x>=1.0))
    result = np.where(mask>0.1,0.0,result)
    result = np.where(mask<-0.1,1.0,result)
    return result
#    return (x>0.0)*(
#            0.5*(1.0+np.tanh(np.tan(np.pi*(x-0.5))))*(x<1.0)
#            +
#            (x>=1.0))
    
class momentsBeta(linearAdvection):
    def init(self,x,nComp):
        U      = np.zeros(np.append(np.shape(x),nComp))
        alpha  = 3.5 + 1.5*np.sin(2.0*np.pi*x)
        beta   = 3.5 - 1.5*np.cos(2.0*np.pi*x)
        #U[...,0] = (16.0*(x**2)*((1.0-x)**2))**2
        U[...,0] = hh(2.0*x)*(x<0.5)+hh(2.0-2.0*x)*(x>=0.5)
        for i in np.arange(nComp-1)+1:
            U[...,i] = U[...,i-1]*(alpha+(i-1.0))/(alpha+beta+(i-1.0))
        return U
    def name(self):
        return "momentsBeta"

class momentsGamma(linearAdvection):
    def init(self,x,nComp):
        from scipy.special import gamma
        U       = np.zeros(np.append(np.shape(x),nComp))
        lambda2min = 0.02;lambda2max = 0.7;
        k2min      = 3.0 ;k2max      = 10.0;
#        w1  = 16.0*x**2*(1.0-x)**2
#        w2  = (x>=0.25)*256.0/81.0*(4.0*x-1.0)**2*(1.0-x)**2
#        w3  = 9.0*(x>=1.0/3.0)*(x<=1.0)*(3.0*x-1.0)**2*(1.0-x)**2
#        w1  = w1**2
#        w2  = w2**2
#        w3  = w3**2
#        #lambda1 = 0.03
#        lambda2 = ((x<=1.0/3.0)*lambda2min
#                  +(x>=2.0/3.0)*lambda2max
#                  +(x>1.0/3.0)*(x<2.0/3.0)*(lambda2min*(2.0-3.0*x)**2*(6.0*x-1.0)
#                                           -lambda2max*(1.0-3.0*x)**2*(6.0*x-5.0))
#                  )
#        #k1      = 2.0
#        k2      = ((x<=1.0/3.0)*k2min
#                  +(x>=2.0/3.0)*k2max
#                  +(x>1.0/3.0)*(x<2.0/3.0)*(k2min*(2.0-3.0*x)**2*(6.0*x-1.0)
#                                           -k2max*(1.0-3.0*x)**2*(6.0*x-5.0))
#                  )        
#        lambda2 = ((x<=1.0/3.0)*lambda2min
#                  +(x>=2.0/3.0)*lambda2max
#                  +(x>1.0/3.0)*(x<2.0/3.0)*(lambda2min+(lambda2max-lambda2min)*(1.0-np.sin((x-2.0/3.0)*np.pi*3.0/2.0)**8))
#                  )
#        k2 = ((x<=1.0/3.0)*k2min
#                  +(x>=2.0/3.0)*k2max
#                  +(x>1.0/3.0)*(x<2.0/3.0)*(k2min+(k2max-k2min)*(1.0-np.sin((x-2.0/3.0)*np.pi*3.0/2.0)**8))
#                  )
#        eps = 0.1
#        lambda2 = lambda2min+(lambda2max-lambda2min)*np.tanh((x-0.5)/eps)
#        k2      = k2min+(k2max-k2min)*np.tanh((x-0.5)/eps)
        w1 = hh(2.0*x)*(x<0.5)+hh(2.0-2.0*x)*(x>=0.5)
        w2 = hh((8.0*x-2.0)/3.0)*(x<5.0/8.0)+hh((8.0-8.0*x)/3.0)*(x>=5.0/8.0)
        w3 = hh(3.0*x-1.0)*(x<2.0/3.0)+hh(3.0-3.0*x)*(x>=2.0/3.0)
        lambda2 = lambda2min + (lambda2max-lambda2min)*hh(3.0*x-1.0)
        k2      =      k2min + (     k2max-     k2min)*hh(3.0*x-1.0)
        xi      = 0.02
        for i in np.arange(nComp):
            U[...,i] = w1*xi**i+w2*(2.0*xi)**i+w3*lambda2**i*gamma(1.0+float(i)/k2)
        return U
    def name(self):
        return "momentsGamma"

class momentsBorder(linearAdvection):
    def init(self,x,nComp):
        from scipy.special import gamma
        U       = np.zeros(np.append(np.shape(x),nComp))
        lambda2min = 0.02;lambda2max = 0.7;
        k2min      = 3.0 ;k2max      = 10.0;
        w1  = 1.00*(hh(4.0*x-0)*(x<0.50)+hh(3.0-4.0*x)*(x>=0.50))
        w2  = 0.50*(hh(4.0*x-1)*(x<0.50)+hh(3.0-4.0*x)*(x>=0.50))
        w3  = 0.25*(hh(4.0*x-2)*(x<0.75)+hh(7.0-8.0*x)*(x>=0.75))
        lambda2 = lambda2min*hh(2.0-2.0*x) + lambda2max*hh(2.0*x-1.0)
        k2      =      k2min*hh(2.0-2.0*x) +      k2max*hh(2.0*x-1.0)
        xi      = 0.05*x
        for i in np.arange(nComp):
            U[...,i] = w1*xi**i+w2*(2.0*xi)**i+w3*lambda2**i*gamma(1.0+float(i)/k2)
        return U
    def name(self):
        return "momentsBorder"

""" **************************************************************"""
""" ****************** Burgers' InitFunctions ********************"""
""" **************************************************************"""

class wave(burgersInit):
    def init(self,x):
        return ((x<=0.25)+(x>0.25)*(x<0.5)*(-4.0*x+2.0))[...,np.newaxis]
    def name(self):
        return "wave"
    def ExactSolution(self,x,t):
        assert(t<0.25)
        return ((x<=0.25+t)+(x>0.25+t)*(x<0.5)*(0.5-x)/(0.25-t))[...,np.newaxis]
        
class BurgersSinus(burgersInit):
    def init(self,x):
        self.shift = 10.0
        return (np.sin(2.0*np.pi*x)+self.shift)[...,np.newaxis]
    def name(self):
        return "BurgersSinus"
    def ExactSolution(self,x,t):
        assert(t<0.5/np.pi) ### Otherwise the solution contains a shock...
        ### remove shift advection     
        xShift = x - self.shift*t
        chi    = np.copy(xShift)
        ### Get the foot of the characteristic for each QP
        error = 1.0
        it    = 0.0
        itMax = 100
        while (error>1.0e-12) & (it<itMax):
            it += 1
            res = (self.F(chi,t)-xShift)/self.FPrime(chi,t)
            error = np.max(np.abs(res))
            chi -= res
        if(it==itMax):
            print "Newton has not converged in Burgers Sinus Exact solution"
            print it, error
            exit
        return (np.sin(2.0*np.pi*(chi-np.floor(chi)))+self.shift)[...,np.newaxis]
    def F(self,chi,t):
        return chi+t*np.sin(2.0*np.pi*chi)
    def FPrime(self,chi,t):
        return 1.0+2.0*np.pi*t*np.cos(2.0*np.pi*chi)
            
""" **************************************************************"""
""" ******************** PGD InitFunctions ***********************"""
""" **************************************************************"""

### linear Advection for PGD
class rhoProfilePGD(PGDInit):
    def init(self,x):
        self.zero = 1.0e-13
        U = np.zeros(np.append(np.shape(x),2))
        ### Rho_0 is any perturbation. You can call any scalar function here.
        U[...,0] = localizedCosine(self.param).init(x)[...,0]
        #U[...,0] = np.maximum(localizedCosine().init(x)[...,0],self.zero)
        ### velocity is 1.0 or something else if you wish
        U[...,1] = U[...,0]
        #U[...,1] = U[...,0]*(1.0-self.zero)
        return U
    def name(self):
        return "rhoProfilePGD"
    def ExactSolution(self,x,t):
        xFinal  = x-t ### Velocity is assumed to be 1.0
        xFinal -= np.floor(xFinal)
        return self.init(xFinal)

### linear Advection for PGD
class rhoProfilePGD10(rhoProfilePGD):
    def init(self,x):
        self.zero = 1.0e-13
        U = np.zeros(np.append(np.shape(x),2))
        ### Rho_0 is any perturbation. You can call any scalar function here.
        U[...,0] = localizedCosine10(self.param).init(x)[...,0]
        #U[...,0] = np.maximum(localizedCosine().init(x)[...,0],self.zero)
        ### velocity is 1.0 or something else if you wish
        U[...,1] = U[...,0]
        #U[...,1] = U[...,0]*(1.0-self.zero)
        return U      
    def name(self):
        return "rhoProfilePGD10"
        
### Collapsing Droplet Clouds
class particlesParcelsPGD(PGDInit):
    def init(self,x):
        U = np.zeros(np.append(np.shape(x),2))
        ### Rho_0 is two symmetric perturbation on both sides of x=0.5
#        U[...,0] = np.sin(2*np.pi*x)**8
        U[...,0] = (np.cos(5*np.pi*(x-0.2))**4*(x>0.1)*(x<0.3)
                  + np.cos(5*np.pi*(x-0.8))**4*(x>0.7)*(x<0.9))
        #U[...,0] = np.maximum(np.sin(2*np.pi*x)**8,self.zero)
        ### velocity is 1.0 on the left, -1.0 on the right
        ### if x=0.5 is one of the Dofs, its velocity is 0.0
        U[...,1] = U[...,0]*(2*(x<0.5)-1.0)*(x!=0.5)
        #U[...,1] = U[...,0]*((1.0-self.zero)*((x<0.5)-(x>=0.5)))
        return U
    def name(self):
        return "particlesParcelsPGD"  
    def ExactSolution(self,x,t):
        return np.zeros(np.append(np.shape(x),2))

""" **************************************************************"""
""" ******************** Euler InitFunctions *********************"""
""" **************************************************************"""
### linear Advection for Euler
class rhoProfileEuler(EulerInit):
    def init(self,x):
        self.zero = 1.0e-13
        U = np.zeros(np.append(np.shape(x),3))
        ### Rho_0 is any perturbation. You can call any scalar function here.
        U[...,0] = localizedCosine(self.param).init(x)[...,0]
        #U[...,0] = np.maximum(localizedCosine().init(x)[...,0],self.zero)
        ### velocity is 1.0 or something else if you wish
        U[...,1] = U[...,0]
        #U[...,1] = U[...,0]*(1.0-self.zero)
        ### Pressure is 1.0
        ### rho E = p/(gamma-1.0)+0.5*rho*u^2
        U[...,2] = 1.0/0.4+0.5*U[...,0]     
        return U
    def name(self):
        return "rhoProfileEuler"
    def ExactSolution(self,x,t):
        xFinal  = x-t ### Velocity is assumed to be 1.0
        xFinal -= np.floor(xFinal)
        return self.init(xFinal)

### Collapsing Droplet Clouds For Euler
class particlesParcelsEuler(EulerInit):
    def init(self,x):
        U = np.zeros(np.append(np.shape(x),3))
        ### Rho_0 is two symmetric perturbation on both sides of x=0.5
        U[...,0] = (np.cos(5*np.pi*(x-0.2))**4*(x>0.1)*(x<0.3)
                  + np.cos(5*np.pi*(x-0.8))**4*(x>0.7)*(x<0.9))
        ### velocity is 1.0 on the left, -1.0 on the right
        ### if x=0.5 is one of the Dofs, its velocity is 0.0
        U[...,1] = U[...,0]*(2*(x<0.5)-1.0)*(x!=0.5)
        ### Initial Pressure is zero. It will be created later.
        ### rho E = p/(gamma-1.0)+0.5*rho*u^2
        U[...,2] = 0.0/0.4+0.5*U[...,0]*((2*(x<0.5)-1.0)*(x!=0.5))**2
        return U
    def name(self):
        return "particlesParcelsEuler"
    def ExactSolution(self,x,t):
        return np.zeros(np.append(np.shape(x),3))
        
### Sod Test Case with a 1-Fan and a 3-Shock
class Sod1(EulerInit):
    def init(self,x):
        self.zero = 1.0e-13
        U = np.zeros(np.append(np.shape(x),3))
        self.Ul = np.array([1.0,0.0,0.26])
        self.Ur = np.array([0.5,0.0,0.13])
        U = (x<0.5)[...,np.newaxis]*self.Ul + (x>=0.5)[...,np.newaxis]*self.Ur
        return U
    def name(self):
        return "Sod1"
    def ExactSolution(self,x,t):
        if(np.abs(t)<1.0e-12):
            return self.init(x)
        ### Solve Riemann Problem
        self.VG,self.sigmaG,self.V1,self.V2,self.sigmaD,self.VD = RiemannEuler(self)
        ### Set inner states in time
        xi = (x-0.5)/t
        UExact = np.zeros(np.append(np.shape(xi),3))
        gamma  = self.gamma
        ### Left State
        rhoG = self.VG[0]
        vG   = self.VG[1]
        pG   = self.VG[2]
        cG   = self.VG[3]
        ### Intermediate States
        rho1  = self.V1[0]
        vStar = self.V1[1]
        pStar = self.V1[2]
        c1    = self.V1[3]
        rho2  = self.V2[0]
        c2    = self.V2[3]
        ### Right State
        rhoD = self.VD[0]
        vD   = self.VD[1]
        pD   = self.VD[2]
        cD   = self.VD[3]
        ### Waves extreme speeds
        sigmaG = self.sigmaG
        sigmaD = self.sigmaD
        ### Compute solution
        v  = ((xi<=sigmaG[0])*vG
             +(xi> sigmaG[0])*(xi< sigmaG[1])*2.0/(gamma+1)*(xi+cG+0.5*(gamma-1.0)*vG)
             +(xi>=sigmaG[1])*(xi<=sigmaD[0])*vStar
             +(xi> sigmaD[0])*(xi< sigmaD[1])*2.0/(gamma+1)*(xi-cD+0.5*(gamma-1.0)*vD)
             +(xi>=sigmaD[1])*vD)
        c  = ((xi<=sigmaG[0])*cG
             +(xi> sigmaG[0])*(xi< sigmaG[1])*(cG+0.5*(gamma-1.0)*(vG-v))
             +(xi>=sigmaG[1])*(xi< vStar)    *c1
             +(xi>=vStar)    *(xi<=sigmaD[0])*c2
             +(xi> sigmaD[0])*(xi< sigmaD[1])*(cD-0.5*(gamma-1.0)*(vD-v))
             +(xi>=sigmaD[1])*cD)
        p  = ((xi<=sigmaG[0])*pG
             +(xi> sigmaG[0])*(xi< sigmaG[1])*pG*(c/cG)**(2.0*gamma/(gamma-1))
             +(xi>=sigmaG[1])*(xi<=sigmaD[0])*pStar
             +(xi> sigmaD[0])*(xi< sigmaD[1])*pD*(c/cD)**(2.0*gamma/(gamma-1))
             +(xi>=sigmaD[1])*pD)
        rho= ((xi<=sigmaG[0])*rhoG
             +(xi> sigmaG[0])*(xi< sigmaG[1])*rhoG*(p/pG)**(1.0/gamma)
             +(xi>=sigmaG[1])*(xi< vStar)    *rho1
             +(xi>=vStar)    *(xi<=sigmaD[0])*rho2
             +(xi> sigmaD[0])*(xi< sigmaD[1])*rhoD*(p/pD)**(1.0/gamma)
             +(xi>=sigmaD[1])*rhoD)
        UExact[...,0] = rho
        UExact[...,1] = rho*v
        UExact[...,2] = p/(gamma-1.0)+0.5*rho*v**2
        return UExact

### Axisymmetrical test case for Valentin. 2017.10.26
class AxisymmetricEuler(EulerInit):
    def init(self,x):
        U = np.zeros(np.append(np.shape(x),3))
        rho_inj = 1.0/11.5
        u_inj   = -11.5
        e_inj   = 0.0
        ### (0:nDofPerCell,0:nCells,0:nVar)
        U[:,-2:,0] = rho_inj
        U[:,-2:,1] = rho_inj*u_inj
        U[:,-2:,2] = rho_inj*(e_inj + 0.5*u_inj**2)
        return U
    def name(self):
        return "axisymmetric"
    def ExactSolution(self,x,t):
        return np.zeros(np.append(np.shape(x),3))
