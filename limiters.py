# -*- coding: utf-8 -*-
"""
Created on Wed Sep 17 12:52:12 2014

@author: larat
"""
import numpy as np

def minmod(a, b):
    return (a * b > 0) * np.sign(a) * np.minimum(np.abs(a), np.abs(b))
