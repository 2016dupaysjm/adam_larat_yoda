#!/usr/bin/env python
import numpy as np
import miscRoutines1D as mR
import mesh1D 
import parameters
import pylab

np.set_printoptions(linewidth=1000)

def Main(N=-1,doPrint=True,doPlot=False):
    p  = parameters.parameters(N)
    if doPrint:
        print("YODA: begin")
        print "Number of Cells: N = ",p.N
        print "RootName is: ", p.rootName
    
    ### Creates Mesh, Quadrature Points and Initializes State Vector m.U
    m  = mesh1D.Mesh1D(p)
    I  = m.GetInnerCellsId()
    eI = m.GetInnerEdgesId()
    ### U has shape [nDofs,nCells+nGhosts,nComp] 
    U  = p.initFunction.initialize(m)
    NormInitCond    = np.abs(m.UBar(U))
    NormInitCond1   = np.sum(NormInitCond,axis=0)*m.dx
    NormInitCond2   = np.sqrt(np.sum(NormInitCond**2,axis=0)*m.dx)
    NormInitCondInf = np.max(NormInitCond,axis=0)
    NormInitCond1   = np.where(NormInitCond1<1.0e-16,1.0,NormInitCond1)
    NormInitCond2   = np.where(NormInitCond2<1.0e-16,1.0,NormInitCond2)
    NormInitCondInf = np.where(NormInitCondInf<1.0e-16,1.0,NormInitCondInf)
    if doPrint: 
	print "L1 Norm = ",NormInitCond1
    	print "L2 Norm = ",NormInitCond2
    	print "LInf Norm = ", NormInitCondInf
    ### Store initial condition for Error Analysis
    #U0 = np.copy(U)
    
    ### Main classes instance of this program. 
    ### Could be removed, but the code is easier to read with these lines
    timeIntegration = p.timeIntegration
    scheme          = p.scheme
    initFunction    = p.initFunction
    equation        = initFunction.equation
    
    p.t      = 0
    p.n      = 0
    p.nbStep = 0
    p.dt     = 0.0
    if doPrint:
        mR.SaveData(U,m)
    
    ### time loop: begin
    while (p.t < p.tMax): 
        mR.updateTimeStep(p,p.CFL*m.dx/np.max(equation.cMax(U[0,eI],U[-1,eI+1])))

        U = timeIntegration.step(U,p.dt,scheme,equation,m)

        p.nbStep += 1
        p.t      += p.dt 
        if doPrint & mR.INeedToSave(p) :
            p.n  += 1
            mR.SaveData(U,m)
    ### time loop: end
    if doPrint:
        print "RootName is: ", p.rootName
        print("YODA: done.")
    ### Compute Errors
    UExact  = initFunction.GetExactSolution(m,p.t)
    UErrBar = np.abs(m.UBar(UExact-U))[...,0]
    if doPlot:
        pylab.close('all')
        #pylab.plot(m.X[I],m.UBar(U0)[I],label="U0")
        pylab.plot(m.X[I],m.UBar(UExact)[I],label="UExact")
        pylab.plot(m.X[I],m.UBar(U)[I],'x-',label="t = "+str(p.t))
        pylab.legend()
        pylab.savefig("FinalSolution.png")
        pylab.figure()
        pylab.plot(m.X[I],UErrBar[I],'x-',label="L1Error")
        pylab.legend()
        pylab.savefig("FinalErrorL1InTheMean.png")
        pylab.figure()
        pylab.plot(m.X[I],m.UBar(np.abs(UExact-U))[I],'x-',label="L1Error")
        pylab.legend()
        pylab.savefig("FinalErrorL1Exact.png")
    if (p.equation.name() == "momentsAdv"):
        return [
                m.dx,
                np.sum(UErrBar[I],axis=0)*m.dx/NormInitCond1,
                np.sqrt(np.sum(UErrBar[I]**2,axis=0)*m.dx)/NormInitCond2,
                np.max(UErrBar[I],axis=0)/NormInitCondInf
               ]
    else:
	return [
		m.dx,
		np.sum(UErrBar[I],axis=0)*m.dx,
		np.sqrt(np.sum(UErrBar[I]**2,axis=0)*m.dx),
		np.max(UErrBar[I],axis=0)
	       ]

### following commands are executed only if main.py is executed. 
### If it is imported, it is not! 
if __name__ == "__main__":  
    dx,Err1,Err2,ErrInf = Main(doPlot=False)
    print "L1 Error = ",Err1
    print "L2 Error = ",Err2
    print "Linf Error = ",ErrInf
