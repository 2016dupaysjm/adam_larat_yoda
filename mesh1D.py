import numpy as np
import quadrature as quad

class Mesh1D:
    def __init__(self,p):
        self.param = p
        ### Mesh size 
        self.xMin = p.xMin
        self.xMax = p.xMax
        ### Number of Inner Cells
        self.nx = p.N
        ### Number of ghost on the boundaries
        self.ng = p.ng
        ### Total Number of cells
        self.nxTot = p.NTot
        ### Some cells IDs
        self.FirstInnerCell = p.ng
        self.LastInnerCell  = p.N + p.ng - 1
        self.FirstInnerEdge = p.ng - 1
        self.LastInnerEdge  = p.ng - 1 + p.N
        ### Cells IDs and Edges IDs
        self.Itot = np.arange(self.nxTot)
        self.eI   = np.arange(self.nxTot - 1)
        ### Delta x
        self.dx = (p.xMax - p.xMin) / p.N
        ### Boundary Conditions
        if(p.BCs == "periodic"):
            self.applyBC = self.PeriodicBC
        elif(p.BCs == "dirichlet"):
            self.applyBC = self.DirichletBC
        elif(p.BCs == "axisymmetric"):
            self.applyBC = self.AxisymmetricBC
        else: 
            print "Unknown boundary conditions. Abort!"
            exit
        ### Coordinates
        self.X    = np.zeros(self.nxTot)
        self.X[self.ng:-self.ng] = np.linspace(self.xMin, self.xMax - self.dx, self.nx)+0.5*self.dx
        self.applyBC(self.X,"coordinates")
        ### Generate appropriate quadratures
        self.nDofs = p.scheme.nDofs()
        self.quads = quad.GaussLobattoQuadrature(self.nDofs)
        ### Store number of components per states (1 in scalar)
        self.nComp = p.initFunction.GetNComp()
        return 
#    
    def Show(self):
        print "nx=", self.nx
        return
#
    def GetAllCellsId(self):
        return self.Itot
#
    def GetAllEdgesId(self):
        return self.eI
#
    def GetInnerCellsId(self):
        return self.Itot[self.FirstInnerCell : self.LastInnerCell + 1]
#
    def GetInnerEdgesId(self):
        return self.eI[self.FirstInnerEdge : self.LastInnerEdge + 1]
#
    def NewCellsArray(self,lines=1,components=1):
        # Creates a matrix with "lines" lines and nxTot columns
        # shape() then always returns a tuplet (lines,nxTot)
        return np.zeros(self.nxTot*lines*components).reshape(lines,self.nxTot,components)
#
    def GetCoords(self):       
        return self.X
#
    def NewEdgesArray(self):
        return np.zeros([self.nxTot - 1])

    def PeriodicBC(self, a, arrayType="solution"):
        IGhost = np.indices([self.ng])
        taille = np.size(np.shape(a))
        if(taille==1):
            a[IGhost] = a[IGhost + self.nx]
            a[IGhost + self.nx + self.ng] = a[IGhost + self.ng]
        elif(taille>=2):
            a[:,IGhost] = a[:,IGhost+self.nx]
            a[:,IGhost+self.nx+self.ng] = a[:,IGhost+self.ng]
        else: 
            print "Unknown Tensor size: ", taille
            print "Abort!"
            exit
    def DirichletBC(self, a, arrayType="solution"):
        IGhost = np.indices([self.ng])
        if(arrayType=="solution"):
            left = np.array([self.xMin])
            right= np.array([self.xMax])
            a[:,IGhost] = self.param.initFunction.init(left)
            a[:,IGhost+self.nx+self.ng] = self.param.initFunction.init(right)
        elif(arrayType=="coordinates"):
            a[IGhost] = self.xMin-0.5*(self.ng-IGhost)*self.dx
            a[IGhost+self.nx+self.ng] = self.xMax+0.5*(self.ng-IGhost)*self.dx
        elif(arrayType=="slopes"):
            a[:,IGhost] = 0.0
            a[:,IGhost+self.nx+self.ng] = 0.0
        else:
            print "Unknown arrayType: ", arrayType
            print "Abort!"
            exit      
    def symmetric(self,a):
        b = np.copy(a)
        b[...,1] *= -1.0
        return b
    def AxisymmetricBC(self, a, arrayType="solution"):
        ### 0 is a symmetry conditon
        ### N is a Dirichlet conditon
        IGhost = np.indices([self.ng])
        if(arrayType=="solution"):
            a[:,IGhost] = self.symmetric(a[:,IGhost+1])
            a[:,IGhost+self.nx+self.ng] = a[:,IGhost+self.nx+self.ng-1]
        elif(arrayType=="coordinates"):
            a[IGhost] = self.xMin-0.5*(self.ng-IGhost)*self.dx
            a[IGhost+self.nx+self.ng] = self.xMax+0.5*(self.ng-IGhost)*self.dx
        elif(arrayType=="slopes"):
            a[:,IGhost] = 0.0
            a[:,IGhost+self.nx+self.ng] = 0.0
        else:
            print "Unknown arrayType: ", arrayType
            print "Abort!"
            exit      
    def GetNComp(self):
        return self.nComp
    def UBar(self,U):
        return np.tensordot(self.quads.GetWeights(),U,1) ### see np.tensordot documentation
            