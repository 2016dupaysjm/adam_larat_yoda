import numpy as np

def INeedToSave(p):
    return (np.floor((p.nbSaveMax + 1e-12) * p.t / p.tMax) >= p.n + 1)

def PrintInfoLine(mass,p):
    if p.nbStep == 0: 
        p.mass0 = mass
    massLoss = (mass-p.mass0) #*(np.abs(mass-p.mass0)>1.0e-12)
    print ("step=" + str(p.nbStep).zfill(6) 
        + "    t=" + ("%.3f" % p.t) + "/" + str(p.tMax) 
        + "    dt=" +("%.3e" % p.dt)
        + "    saved data=" + str(p.n).zfill(p.nbDigit) + "/" + str(p.nbSaveMax).zfill(p.nbDigit) 
        + "    Mass Loss="), massLoss # + ("%+.3e" % massLoss[0])
    return

def SaveData(U,m):
    p    = m.param
    fileName = p.rootName + str(p.n).zfill(p.nbDigit) + ".dat"
    I    = m.GetInnerCellsId()
    x    = m.GetCoords()
    UBar = m.UBar(U)
    if (p.initFunction.name() == "BurgersSinus") | (p.initFunction.name() == "Sod1") | (p.equation.name() == "momentsAdv"):
#        print "Exact solution stored at time ", p.t
#        UExactOut = np.reshape(p.initFunction.GetExactSolution(m,p.t),(3,12))
#        np.savetxt("Exact.dat",np.transpose(UExactOut)[I])
        UExact = p.initFunction.GetExactSolution(m,p.t)
        out    = np.column_stack((x[I], np.where(np.abs(UBar[I])<1.0e-12,0.0,UBar[I]),m.UBar(UExact)[I]))
    else:
        out  = np.column_stack((x[I], np.where(np.abs(UBar[I])<1.0e-12,0.0,UBar[I])))
    np.savetxt(fileName, out, header=" It "+("%06d"%m.param.nbStep)+", time "+ ("%.8f"%p.t))
    if (p.equation.name() == "momentsAdv"):
        for i in np.arange(np.shape(U)[0]):
            out  = np.column_stack((x[I]-0.5*p.dx,U[i,I],UExact[i,I]))
            np.savetxt(fileName+str(i),out,header=" time "+ ("%.8f"%p.t))
    PrintInfoLine(sum(UBar[I])*m.dx,p)
    return
        
def updateTimeStep(p,dt):
    p.dt = dt 
    if(p.tMax-p.t-p.dt<1e-12):
        p.dt = p.tMax-p.t
