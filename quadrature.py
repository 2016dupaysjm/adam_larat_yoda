# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 16:14:55 2015

@author: larat
"""
import numpy as np
import scipy.special as s

class GaussLobattoQuadrature:
    def __init__(self,Nq):
        self.nq = Nq
        if Nq == 1:
            self.wq = np.array([1.0])
            self.Xq = np.array([0.0])        
        elif Nq == 2:
            self.wq = np.array([0.5,0.5])
            self.Xq = np.array([-1.0,1.0])
        elif Nq == 3:
            self.wq = np.array([1.0/6.0,2.0/3.0,1.0/6.0])
            self.Xq = np.array([-1.0,0.0,1.0])
        else:
            [self.Xq,self.wq] = GaussLobattoQuadPts(Nq)
            self.wq *= 0.5
    def GetSize(self):
        return self.nq
    def GetWeights(self):
        return self.wq
    def GetCoords(self):
        return self.Xq
        
### sorts eigenvalues and weights so that x[0]<x[1]<…<x[N]
def sort(x,w):
    N = np.size(x)
    for i in np.arange(N-1):
        if x[i] > x[i+1]:
            j = i
            while x[j]>x[j+1]:
                interX = x[j]
                interW = w[j]
                x[j] = x[j+1]
                w[j] = w[j+1]
                x[j+1] = interX
                w[j+1] = interW
                j-=1
                if j<0:
                    break
    return [x,w]
    
### returns the values of the N+1 first orthonormal (alpha,beta) Jacobi polynomials 
### at absissas x.
### Special values alpha=beta=0 gives Legendre Polynomials. 
### Also P_{n}^{(alpha,beta)}'(x) = sqrt(n*(n+alpha+beta+1))*P_{n-1}^{(alpha+1,beta+1)}(x)
def JacobiPolynomialsAtX(x,alpha,beta,N):
    dim    = np.size(x)    
    I      = np.arange(np.float(N+1))
    inter  = 2*I+alpha+beta
    A      = 2.0/inter*np.sqrt(I*(I+alpha+beta)*(I+alpha)*(I+beta)/(inter-1)/(inter+1))
    B      = -(alpha**2-beta**2)/inter/(inter+2)
    values = np.zeros([N+1,dim])
    ab1    = np.float(alpha+beta+1)
    gamma0 = 2**(ab1)/ab1*s.gamma(alpha+1)*s.gamma(beta+1)/s.gamma(ab1)
    gamma1 = (alpha+1)*(beta+1)/(ab1+2)*gamma0
    values[0] = 1.0/np.sqrt(gamma0)
    values[1] = 0.5*((ab1+1)*x+alpha-beta)/np.sqrt(gamma1)
    n = 2
    while n<=N:
        values[n] = ((x-B[n-1])*values[n-1]-A[n-1]*values[n-2])/A[n]
        n = n+1
    return values
    
### returns the values of the N+1 first orthonormal Legendre polynomials 
### at absissas x
def LegendrePolynomialsAtX(x,N):
    dim    = np.size(x)    
    I      = np.arange(np.float(N+1))
    A      = np.sqrt(I**2/((2*I+1)*(2*I-1)))
    values = np.zeros([N+1,dim])
    values[0,:] = 1.0/np.sqrt(2.0)
    values[1]   = np.sqrt(1.5)*x
    n = 2
    while n<=N:
        values[n] = (x*values[n-1]-A[n-1]*values[n-2])/A[n]
        n = n+1
    return values
    
### returns the absissas and associated weights 
### of the N Gauss-Legendre quad. points. 
### Integral quadrature \int_{-1}^{1} f(t)dt = \sum_{i=1}^N w_i f(x_i) 
### is exact for polynomials of degree <= 2*N-1
def GaussLegendreQuadPts(N):
    if N<=0:
        print "The number of quad points need to be > 0. Please provide a different N. N=", N
        exit
    I = np.arange(np.float(N))
    A = np.sqrt(I**2/((2*I+1)*(2*I-1)))
    J = np.diag(A[1:],-1)+np.diag(A[1:],1)
    [x,V] = np.linalg.eig(J)
    # V[:,i] is the i-th normalized eigenvector associated to x_i
    # In fact, one can demonstrate that the normalization factor is \omega_i
    # and thus w_i = V[0,i]**2/p_0(x_i)**2 = 2.0*V[0,i]**2
    # see "Calculation of Gauss Quadrature Rules", Golub and Welsch 1968.    
    w = 2.0*V[0]**2
    return sort(x,w)

### returns the absissas and associated weights 
### of the N Jacobi quad. points. for alpha and beta > -1, alpha+beta .neq. -1
### alpha = beta = 0 gives the Gauss-Legendre quad. pts.
### This function is useful for Gauss-Lobatto quad. pts., since the interior 
### points are the roots of Pn-1'(x) = J{n-2,alpha+1,beta+1}
def JacobiQuadPts(alpha,beta,N):
    if(N==0): 
        print "The number of quad points need to be > 0. Please provide a different N. N=", N
        exit
    I = np.arange(np.float(N))
    inter = 2*I+alpha+beta
    A = 2.0/inter*np.sqrt(I*(I+alpha+beta)*(I+alpha)*(I+beta)/(inter-1)/(inter+1))
    B = -(alpha**2-beta**2)/inter/(inter+2)
    J = np.diag(A[1:],-1)+np.diag(B)+np.diag(A[1:],1)
    if((alpha+beta)<1.0e-12):
        J[0,0] = 0.0
    [x,V] = np.linalg.eig(J)
    w = V[0]**2*2**(alpha+beta+1)/(alpha+beta+1)*s.gamma(alpha+1)*s.gamma(beta+1)/s.gamma(alpha+beta+1)
    return sort(x,w)
    
### returns the absissas and associated weights 
### of the N Gauss-Lobatto quad. points. 
### Integral quadrature \int_{-1}^{1} f(t)dt = \sum_{i=1}^N w_i f(x_i) 
### is exact for polynomials of degree <= 2*N-3
def GaussLobattoQuadPts(N):
    if N<2:
        print "The number of quad points need to be > 2. Please provide a different N. N=", N
        exit
    if N==2:
        return [np.array([-1.0,1.0]),np.array([1.0,1.0])]
    [x,V] = JacobiQuadPts(1,1,N-2)
    w = np.zeros(N)
    vals = LegendrePolynomialsAtX(x,N-1)
    w[1:N-1] = 2.0*N*(N-1)/(2*N-1)*vals[N-1]**2
    w[0] = w[N-1] = N*(N-1)
    return [np.append(np.append(-1.0,x),1.0), 2.0/w]    

### The value of the N Gauss-Lobatto Lagrange polynomials at abscissas X
### The i-th GL Lagrange polynomial writes
### L_i^{GL}(x) = \prod_{j\neq i} ((x-x_j)/(x_i-x_j))
def GaussLobattoPolynomialsAtX(x,N):
    [xGL,wGL] = GaussLobattoQuadPts(N)
    theta     = np.ones([N,np.size(x)])
    for j in np.arange(N):
        inter = x-xGL[j]
        for i in np.arange(N):
            if i==j: 
                continue
            theta[i] *= inter/(xGL[i]-xGL[j])
    return theta
    
### The value of the derivativo of the N Gauss-Lobatto Lagrange polynomials at abscissas X
### The derivative of the i-th GL Lagrange polynomial writes
### d_x[L_i^{GL}](x) = \sum_{j\neq i}\prod_{k\neq i & k\neq j} (x-x_k)/\prod_{j\neq i} (x_i-x_j)
def DerGaussLobattoPolynomialsAtX(x,N):
    dim       = np.size(x)
    [xGL,wGL] = GaussLobattoQuadPts(N)
    theta     = np.zeros([N,dim])
    for i in np.arange(N):
        inter = np.ones([N,dim])
        coeff = 1.0
        for k in np.arange(N):
            if k==i :
                inter[i] *= 0.0
                continue
            coeff *= (xGL[i]-xGL[k])
            for j in np.arange(N):
                if j==k:
                    continue
                inter[j] *= (x-xGL[k])
        theta[i] = np.sum(inter,axis=0)/coeff        
    return theta
    
### Compute Mass Matrix for N-th order Gauss-Lobatto Lagrange basis functions
### Watch out, there are (N+1) of them!!!
def MassMatrixGL(N,NQuads=0):
    if NQuads == 0: 
        NQuads = N
    [x,w] = GaussLegendreQuadPts(NQuads)
    theta = GaussLobattoPolynomialsAtX(x,N)
    M     = np.zeros([N,N])
    for i in np.arange(N):
        for j in np.arange(N):
            M[i,j] = np.sum(w*theta[i]*theta[j])
    return M
    
def StiffnessMatrixGL(N,NQuads=0):
    if NQuads == 0:
    ### theta is of order N-1 and dTheta of order N-2. theta*dTheta is 
    ### correctly integrated with N-1 Gauss-Legendre quad points
        NQuads = N-1 
    [x,w]  = GaussLegendreQuadPts(NQuads)
    theta  = GaussLobattoPolynomialsAtX(x,N)
    dTheta = DerGaussLobattoPolynomialsAtX(x,N)
    K = np.zeros([N,N])
    for i in np.arange(N):
        for j in np.arange(N):
            K[i,j] = np.sum(w*theta[i]*dTheta[j])
    return K
    
# test des fonctions 
if __name__ == "__main__":
    N = 3
    xGL,wGL = GaussLobattoQuadPts(N)
    print xGL
    print wGL
    wGL*=0.5
    MGL     = MassMatrixGL(N)    
    KGL     = StiffnessMatrixGL(N)
    UnMM    = np.zeros([N,N]); UnMM[ 0, 0] = 1.0
    UnPP    = np.zeros([N,N]); UnPP[-1,-1] = 1.0
    uAdv    = 0.5
    mat     = np.kron(UnPP-np.transpose(KGL),MGL)+ uAdv*np.kron(MGL,KGL)
    matReduced = mat[N:,N:]
    print "Mass Matrix:"
    print MGL
    print "Stiffness Matrix:"
    print KGL
    print "System Matrix (mat):"
    print mat
    print "Reduced Matrix:"
    print matReduced
    print "inverses: mat"
    invMat=np.linalg.inv(mat)
    print invMat
    invRed=np.linalg.inv(matReduced)
    print "Reduced Mat"
    print invRed
    print "Coeffs Reduced"
    coeffs = invRed.dot(-mat[N:,:N])
    print coeffs
    coeffWeak = invMat.dot(np.kron(UnMM,MGL))
    print "Coeffs weak form"
    #print coeffWeak
    print "Look at mean value at each time Level as a function of the solution at t^n"
    coeffsTrans = coeffs.reshape(N-1,N,N).transpose([0,2,1]).reshape(N*(N-1),N)
    print coeffsTrans.dot(wGL).reshape(N-1,N)
    
#    print "******************************************"
#    print "** New Locally conservative Formulation **"
#    print "******************************************"
#    UnPM = np.zeros((N,N)); UnPM[-1, 0] = 1.0
#    UnMP = np.zeros((N,N)); UnMP[ 0,-1] = 1.0
#    matCons = np.kron(UnPP-np.transpose(KGL),MGL)+uAdv*np.kron(MGL,UnPP-UnMP-KGL.T)
#    print "New conservative matrice" 
#    print matCons
#    print "its inverse"
#    invMatCons = np.linalg.inv(matCons[N:,N:])
#    print invMatCons
#    print "Conservative coefficients function of Q(t^n,x)"
#    coeffsCons = invMatCons.dot(-matCons[N:,:N])
#    print coeffsCons
#    print "Look at mean value at each time Level as a function of the solution at t^n"
#    coeffsConsTrans = coeffsCons.reshape(N-1,N,N).transpose([0,2,1]).reshape(N*(N-1),N)
#    print coeffsConsTrans.dot(wGL).reshape(N-1,N)
    

    
    
    