import numpy as np
import limiters
from scipy import linalg
import quadrature as quad

### Mother Class. All the schemes derive from this class
class scheme:
    def __init__(self):
        return
    def RHS(self,U,eq,m):
        return
    def name(self):
        return ""
    def positivityLimitation(self,U,eq,m):
        return U
    def nDofs(self):
        return 1
    def usesFStar(self):
        return False
    
""" *************************************************
    Finite Differences
    *************************************************"""
    
""" ******************** Upwind *********************"""

class upwind(scheme): 
    def RHS(self,U,eq,m):
        I            = m.GetInnerCellsId()
        numFlux      = np.zeros(np.shape(U))
        numFlux[:,I] = -(eq.flux(U[:,I])-eq.flux(U[:,I-1]))/m.dx
        return numFlux
    
    def name(self):
        return "upwind"
        
""" ******************* Centered ********************"""

class centered(scheme): 
    def RHS(self,U,eq,m):
        I            = m.GetInnerCellsId()
        numFlux      = np.zeros(np.shape(U))
        numFlux[:,I] = -(eq.flux(U[:,I+1])-eq.flux(U[:,I-1]))/(2.0*m.dx)
        return numFlux
    def name(self):
        return "centered"

""" ******************* Downwind ********************"""

class downwind(scheme): 
    def RHS(self,U,eq,m):
        I            = m.GetInnerCellsId()
        numFlux      = np.zeros(np.shape(U))
        numFlux[:,I] = -(eq.flux(U[:,I+1])-eq.flux(U[:,I]))/(m.dx)
        return numFlux
    def name(self):
        return "downwind"
    
""" *************************************************
    Finite Volumes
    *************************************************"""
    
""" ******************* Godunov (FV-O1) ********************"""
class Godunov(scheme):
    def RHS(self,U,eq,m):
        I  = m.GetInnerCellsId()
        eI = m.GetInnerEdgesId()
        numFlux      = np.zeros(np.shape(U))
        f_star       = eq.Godunov(U[0,eI],U[0,eI+1]) 
        numFlux[0,I] = -(f_star[I]-f_star[I-1])/m.dx
        return numFlux
    def name(self):
        return "Godunov"
        
""" ******************* LaxFriedrichs (FV-O1) ********************"""
class LaxFriedrichs(scheme):
    def RHS(self,U,eq,m):
        I  = m.GetInnerCellsId()
        eI = m.GetInnerEdgesId()
        numFlux      = np.zeros(np.shape(U))
        f_star       = eq.LaxFriedrichs(U[0,eI],U[0,eI+1]) 
        numFlux[0,I] = -(f_star[I]-f_star[I-1])/m.dx
        return numFlux
    def name(self):
        return "LaxFriedrichs"
        
""" ******************* MUSCL Reconstruction ********************"""
### 1- Watch out, this depend on the numerical flux computation 
### in the "eq" object. See equations.py and the FStar() functions
### 2- Watch out, strongly depends on the choice of the Limiter
### The limiter choice should appear in parameters though... TODO!!!
class MUSCL(scheme):
    def RHS(self,U,eq,m):
        I  = m.GetInnerCellsId()
        eI = m.GetInnerEdgesId()
        numFlux      = np.zeros(np.shape(U))
        ### Watch out, Delta is a single array when U is a matrix even when
        ### nDofs is set to 1. 
        Delta        = np.zeros(np.shape(U))
        ### Verify that the limiters work in vectorial mode...
        Delta[0,I]   = limiters.minmod(U[0,I+1]-U[0,I],U[0,I]-U[0,I-1])
        m.applyBC(Delta,"slopes")
        f_star       = eq.FStar(U[0,eI]+0.5*Delta[0,eI],U[0,eI+1]-0.5*Delta[0,eI+1]) 
        numFlux[0,I] = -(f_star[I]-f_star[I-1])/m.dx
        return numFlux
    def name(self):
        return "MUSCL" 
    def usesFStar(self):
        return True
                
""" ******************* MUSCL-Hancock Scheme ********************"""
### Watch out, strongly depends on the choice of the Limiter
### The limiter choice should appear in parameters though... TODO!!!
class MusclHancock(scheme):
    def RHS(self,U,eq,m):
        I  = m.GetInnerCellsId()
        eI = m.GetInnerEdgesId()
        numFlux      = np.zeros(np.shape(U))
        ### Watch out, Delta is a single array when U is a matrix even when
        ### nDofs is set to 1. 
        Delta        = np.zeros(np.shape(U))
        ### Verify that the limiters work in vectorial mode...
        Delta[0,I]   = limiters.minmod(U[0,I+1]-U[0,I],U[0,I]-U[0,I-1])
        m.applyBC(Delta,"slopes")
        ### Corrected Hancock state to obtain second order in time
        ### UBar^{\pm} = U^{\pm}-dt/dx*[flux(U^+)-flux(U^-)]
        dt = m.param.dt
        Correction = -0.5*dt/m.dx*(eq.flux(U+0.5*Delta)-eq.flux(U-0.5*Delta))
        UBar = m.NewCellsArray(2,m.GetNComp())
        UBar[0] = U[0] - 0.5*Delta[0] + Correction[0] 
        UBar[1] = U[0] + 0.5*Delta[0] + Correction[0] 
        ### Numerical Flux
        f_star       = eq.FStar(UBar[1,eI],UBar[0,eI+1]) 
        numFlux[0,I] = -(f_star[I]-f_star[I-1])/m.dx
        return numFlux
    def name(self):
        return "MusclHancock" 
    def usesFStar(self):
        return True
        
""" *************************************************
    Two-Step Taylor Galerkin
    *************************************************"""
### TTGC is a two-step taylor in time, Galerkin in space 
### Space-Time integration. Should be used only with ForwardEuler in Time
### To be tested!!!
class TTGC(MusclHancock):
    def __init__(self,p,gamma=0.01):
        dx = p.dx; N = p.N; ng=p.ng
        ### Coefficients of the TTGC update
        self.alpha    = 0.5-gamma
        self.beta     = 1.0/6.0
        self.theta1   = 0.0
        self.theta2   = 1.0
        self.epsilon1 = gamma
        self.epsilon2 = 0.0        
        ### Mesh parameters. 
        self.dx = dx 
        self.N  = N
        self.ng = ng
        self.NTot = N + 2*ng
        ### Fill up matrices
        Id = np.eye(N)
        L1 = np.eye(N,k=-1)+np.eye(N,k=N-1)
        U1 = np.eye(N,k=1)+np.eye(N,k=-N+1)
        self.M = np.eye(self.NTot)
        self.T = np.zeros((self.NTot,self.NTot))
        self.D = np.zeros((self.NTot,self.NTot))
        self.M[ng:N+ng,ng:N+ng] = dx/6.0*(4*Id+L1+U1)
        self.T[ng:N+ng,ng:N+ng] = 0.5*(U1-L1)
        self.D[ng:N+ng,ng:N+ng] = 1.0/dx*(2*Id-L1-U1)
        ### Apply Boundary conditions
        if(p.BCs=="dirichlet"):
            self.M[1,0]   = self.M[1,-2]            
            self.M[-2,-1] = self.M[-2,1]
            self.M[1,-2]  = 0.0
            self.M[-2,1]  = 0.0
            self.T[1,0]   = self.T[1,-2]
            self.T[-2,-1] = self.T[-2,1]
            self.T[1,-2]  = 0.0
            self.T[-2,1]  = 0.0
            self.D[1,0]   = self.D[1,-2]
            self.D[-2,-1] = self.D[-2,1]
            self.D[1,-2]  = 0.0
            self.D[-2,1]  = 0.0
        ### Invert M once and for all
        self.Mm1 = linalg.inv(self.M)
        return
    def RHS(self,U,eq,m):
        dt = m.param.dt
        ### UTilde = Un - dt*Mm1*(alpha*T*f(Un)+beta*dt*D*Jac(Un)*f(Un))
        ### Unp1   = Un - dt*Mm1*(theta2*T*f(UTilde)+epsilon1*dt*D*Jac(Un)*f(Un))
        ### Tensor Shapes should be: 
        ###   U,f(U): (1,N,m)
        ###   Jac(U): (1,N,m,m)
        ###   M,D,T:  (N,N)
        fU   = eq.flux(U)[0] ### By taking indice 0, we remove first axe
        JacU = eq.Jac(U)[0]  ### Useful for forthcoming matrix multiply
        ### This next formula is quite obscure. Explanation:
        ### If shape(JacU) = (order,N,m,m) and shape(fU) = (order,N,m) what it does is
        ### product_{ijk} = sum_q [JacU_{ijkq}*fU_{ijq}]
        ### The trick is that now we can have as many indices as wanted at the beginning
        dissip  = np.dot(self.D,np.einsum('...kq,...q->...k',JacU,fU))
        UTilde  = U - dt*np.dot(self.Mm1,
                                     self.alpha*np.dot(self.T,fU)
                                     +
                                     self.beta*dt*dissip
                                    )
        fUTilde = eq.flux(UTilde)[0]
        return -np.dot(self.Mm1,
                      self.theta2*np.dot(self.T,fUTilde)
                      +
                      self.epsilon1*dt*dissip)
    ### def nDofs(self): ### same as MusclHancock
    def name(self):
        return "TTGC"
    def usesFStar(self):
        return False
        
        
""" *************************************************
    Discontinuous Galerkin
    *************************************************"""
### Generic DG class
class DG(scheme):
    def __init__(self):
        return
    def RHS(self,U,eq,m):
        vol = self.volumeTerm(U,eq,m)
        edg = self.edgeFlux(U,eq,m)
        return edg+vol        
        #return vol+edg 
        #return self.volumeTerm(U,eq,m)+self.edgeFlux(U,eq,m)
    def volumeTerm(self,U,eq,m):
        return np.zeros(np.shape(U))
    def edgeFlux(self,U,eq,m):
        numFlux = np.zeros(np.shape(U))
        eI      = m.GetInnerEdgesId()
        f_star  = eq.FStar(U[0,eI],U[0,eI+1])
        ### dt U_l = -1.0/Dx*(F_{i+1/2}-F_{i-1/2}
        I  = m.GetInnerCellsId()
        numFlux[0,I] = -1.0/m.dx*(f_star[I]-f_star[I-1])
    def positivityLimitation(self,U,eq,m):
        return eq.positivityLimitation(U,m)
    def nDofs(self):
        return 1     
    def name(self):
        return "DG" 
    def usesFStar(self):
        return True
        
""" ******************** Order 2 ********************"""
class DG2(DG): 
    def volumeTerm(self,U,eq,m):
        vol = np.zeros(np.shape(U))
        # Only valid for Linear Advection. Quadrature Integrals must be provided
        vol[0] = -3.0*np.sum(eq.flux(U),axis=0)/m.dx
        vol[1] = -vol[0]
        return vol
    def edgeFlux(self,U,eq,m):
        numFlux = np.zeros(np.shape(U))
        eI = m.GetInnerEdgesId()
        f_star = eq.FStar(U[1,eI],U[0,eI+1]) 
        # dt U_l = 2/Dx*(2*F_{i-1/2}+F_{i+1/2}
        I  = m.GetInnerCellsId()
        numFlux[0,I] = 2.0/m.dx*(2.0*f_star[I-1]+f_star[I])
        # dt U_r = -2/Dx*(2*F_{i-1/2}+F_{i+1/2}
        numFlux[1,I] =-2.0/m.dx*(f_star[I-1]+2.0*f_star[I])
        return numFlux
    def nDofs(self):
        return 2       
    def name(self):
        return "DG2"    
        
""" ******************** Order 3 ********************"""
class DG3(DG): 
    def volumeTerm(self,U,eq,m):
        vol = np.zeros(np.shape(U))
        # Only valid for Linear Advection. Quadrature Integrals must be provided
        vol[0] = 1.5/m.dx*(-4.0    *eq.flux(U[0])-8.0/3.0*eq.flux(U[1])+8.0/3.0*eq.flux(U[2]))
        vol[1] = 1.5/m.dx*( 5.0/3.0*eq.flux(U[0])                      -5.0/3.0*eq.flux(U[2]))
        vol[2] = 1.5/m.dx*(-8.0/3.0*eq.flux(U[0])+8.0/3.0*eq.flux(U[1])+4.0    *eq.flux(U[2]))
        return vol
    def edgeFlux(self,U,eq,m):
        numFlux = np.zeros(np.shape(U))
        I  = m.GetInnerCellsId()
        eI = m.GetInnerEdgesId()
        f_star = eq.FStar(U[2,eI],U[0,eI+1]) 
        # dt U^0 = -3/(2*Dx)*(-6.0*F_{i-1/2}+2.0*F_{i+1/2}
        numFlux[0,I] =-1.5/m.dx*(-6.0*f_star[I-1]+2.0*f_star[I])
        # dt U^1 = -3/(2*Dx)*(     F_{i-1/2}-    F_{i+1/2}
        numFlux[1,I] =-1.5/m.dx*(     f_star[I-1]-    f_star[I])
        # dt U^2 = -2/Dx*(2*F_{i-1/2}+F_{i+1/2}
        numFlux[2,I] =-1.5/m.dx*(-2.0*f_star[I-1]+6.0*f_star[I])
        return numFlux
    def nDofs(self):
        return 3       
    def name(self):
        return "DG3"
        
        
""" *************************************************
    Arbirtrary High Order One-Step DG: AHOJ
    *************************************************"""

class AHOJ(scheme):
    ### Constructor. Like for TTGC, builds mass matrix and necessary algebraic
    ### tools for the predictor and integral quadratures
    def __init__(self,p,Nq):
        p.Nq      = Nq
        p.CFL    *= 2.0/(Nq*(Nq+1))
        self.Nq   = Nq 
        self.M    = quad.MassMatrixGL(Nq)
        self.Mm1  = np.linalg.inv(self.M)
        self.K    = quad.StiffnessMatrixGL(Nq)
        self.Kt   = np.transpose(self.K)
        self.BigR   = np.kron(np.transpose(self.K),self.M)
        self.BigR[-Nq:,-Nq:] -= self.M
        self.BigRm1 = np.linalg.inv(self.BigR[Nq:,Nq:])
        self.BigL   = np.kron(self.M,self.K)
        self.N     = p.N
        self.NTot  = p.NTot
        self.dx    = p.dx
        self.CFL   = p.CFL
        [self.xGL,self.wGL] = quad.GaussLobattoQuadPts(Nq) 
    ### Compute the Right-Hand-Side for AHOJ scheme
    def RHS(self,U,eq,m):
        p  = m.param
        Qh       = self.predictor(p,eq,U) 
        FStar    = eq.FStar(np.roll(Qh[-1],1,axis=1),Qh[0]) ### FStar(Ul,Ur)
        FStarInt = np.tensordot(FStar,self.wGL,axes=([0],[0]))
        FStarTerm = np.zeros([self.Nq,self.NTot,eq.GetNComp()])
        FStarTerm[0] = -FStarInt
        FStarTerm[-1] = np.roll(FStarInt,-1,axis=0)
        VolumeTerm = np.tensordot(self.Kt,np.tensordot(self.wGL,eq.flux(Qh),axes=([0],[1])),axes=([1],[0]))  
        return np.tensordot(self.Mm1,VolumeTerm-FStarTerm,axes=([1],[0]))/self.dx    
    ### Predictor gives a first local estimate of the solution by a contracting 
    ### procedure coming from a Galerkin formulation, continuous in space, 
    ### discontinuous in time    
    def predictor(self,p,eq,U):
        flux = eq.flux
        Nq   = p.Nq
        NComp = eq.GetNComp()
        dx   = p.dx
        dt   = p.dt
        ### Qh is shaped into (Nq_T*Nq_X,NTot_Cells,NComponents) 
        Qh = np.zeros([Nq*Nq,p.NTot,NComp])
        for i in np.arange(p.NTot):
            q0   = U[:,i,:]
            Qh[:,i,:] = np.kron(np.ones([Nq,1]),q0)
            Qtau = Qh[:,i,:]
            it = 0
            error = 1.0
            while (it<100) & (error>1.0e-12):
                it += 1
                Qnew = np.dot(self.BigRm1,dt/dx*np.dot(self.BigL[Nq:],flux(Qtau))-np.dot(self.BigR[Nq:,:Nq],q0))
                error = np.sum(abs(Qtau[Nq:]-Qnew))
                Qtau[Nq:] = Qnew
            if it==100:
                print "predictor has reached end of loop, i = ", i, ". Error = ", error 
                while (it<103) & (error>1.0e-12):
                    it += 1
                    Qnew = np.dot(self.BigRm1,dt/dx*np.dot(self.BigL[Nq:],flux(Qtau))-np.dot(self.BigR[Nq:,:Nq],q0))
                    error = np.sum(abs(Qtau[Nq:]-Qnew))
                    Qtau[Nq:] = Qnew
                    print it, error
        return np.transpose(Qh.reshape(Nq,Nq,p.NTot,NComp),(1,0,2,3))
    def nDofs(self):
        return self.Nq       
    def name(self):
        return "AHOJ"+str(self.Nq)
    def usesFStar(self):
        return True
            