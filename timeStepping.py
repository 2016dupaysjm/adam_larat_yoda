import numpy as np

### Mother Class
class timeStep:
    def __init_(self):
        return
    def step(self,U,dt,scheme,eq,m):
        return U
    def name(self):
        return ""    

""" ******************** Forward Euler *********************"""

class FE(timeStep):
    def step(self,U,dt,scheme,eq,m):
        U += dt*scheme.RHS(U,eq,m)
        U = scheme.positivityLimitation(U,eq,m)
        m.applyBC(U)
        return U
    def name(self):
        return "FE"
    
""" ******************** Generic SSP RK *********************"""
class RKSSP(timeStep):
    def __init__(self,k):
        self.k   = k
        if k==1:
            self.aRK = np.array([0.0])
            self.bRK = np.array([1.0])
        elif k==2:
            self.aRK = np.array([0.0,0.5])
            self.bRK = np.array([1.0,0.5])
        elif k==3:
            self.aRK = np.array([0.0,0.75,1.0/3.0])
            self.bRK = np.array([1.0,0.25,2.0/3.0])
        else:
            print "%d-th order SSP Runge Kutta Method has not been implemented yet!"%k
            print "Abort!"
            exit
    def step(self,U,dt,scheme,eq,m):
        U_inter = U
        for i in np.arange(self.k):
            U_inter = (self.aRK[i]*U 
                     + self.bRK[i]*(U_inter+dt*scheme.RHS(U_inter,eq,m)))
            U_inter = scheme.positivityLimitation(U_inter,eq,m)
            m.applyBC(U_inter)
        return U_inter
    def name(self):
        return "RK%02dSSP"%self.k
        
""" ******************** Runge-Kutta 2 *********************"""

class RK2Direct(timeStep):
    def step(self,U,dt,scheme,eq,m):
        U_demi = U + 0.5*dt*scheme.RHS(U,eq,m)
        U_demi = scheme.positivityLimitation(U_demi,eq,m)
        m.applyBC(U_demi)
        U += dt*scheme.RHS(U_demi,eq,m)
        U  = scheme.positivityLimitation(U,eq,m)
        m.applyBC(U)
        return U
    def name(self):
        return "RK2Direct"

class RK2SSP(timeStep):
    def step(self,U,dt,scheme,eq,m):
        U_demi = U + dt*scheme.RHS(U,eq,m)
        m.applyBC(U_demi)
        U_demi = scheme.positivityLimitation(U_demi,eq,m)
        U = 0.5*U + 0.5*(U_demi + dt*scheme.RHS(U_demi,eq,m))
        m.applyBC(U)
        U = scheme.positivityLimitation(U,eq,m)
        return U
    def name(self): 
        return "RK2SSP"
        
""" ******************** Runge-Kutta 3 *********************"""

class RK3SSP(timeStep):
    def step(self,U,dt,scheme,eq,m):
        U_inter = U + dt*scheme.RHS(U,eq,m)
        U_inter = scheme.positivityLimitation(U_inter,eq,m)
        m.applyBC(U_inter)
        U_inter = 0.75*U + 0.25*(U_inter + dt*scheme.RHS(U_inter,eq,m))
        U_inter = scheme.positivityLimitation(U_inter,eq,m)
        m.applyBC(U_inter)
        U = U/3.0 + 2.0/3.0*(U_inter + dt*scheme.RHS(U_inter,eq,m))
        U  = scheme.positivityLimitation(U,eq,m)
        m.applyBC(U)
        return U
    ########################################################
#        U0 = U + dt*scheme.RHS(U,eq,m)
#        U1 = scheme.positivityLimitation(U0,eq,m)
#        m.applyBC(U0)
#        m.applyBC(U1)
#        U00 = 0.75*U + 0.25*(U0 + dt*scheme.RHS(U0,eq,m))
#        U10 = 0.75*U + 0.25*(U1 + dt*scheme.RHS(U1,eq,m))
#        #U01 = scheme.positivityLimitation(U00,eq,m)
#        U11 = scheme.positivityLimitation(U10,eq,m)
#        m.applyBC(U00)
#        #m.applyBC(U01)
#        m.applyBC(U10)
#        m.applyBC(U11)
#        U000 = U/3.0 + 2.0/3.0*(U00 + dt*scheme.RHS(U00,eq,m))
#        #U010 = U/3.0 + 2.0/3.0*(U01 + dt*scheme.RHS(U01,eq,m))
#        U100 = U/3.0 + 2.0/3.0*(U10 + dt*scheme.RHS(U10,eq,m))
#        U110 = U/3.0 + 2.0/3.0*(U11 + dt*scheme.RHS(U11,eq,m))
#        #U001  = scheme.positivityLimitation(U000,eq,m)
#        #U011  = scheme.positivityLimitation(U010,eq,m)
#        #U101  = scheme.positivityLimitation(U100,eq,m)
#        U111  = scheme.positivityLimitation(U110,eq,m)
#        m.applyBC(U000)
#        #m.applyBC(U001)
#        #m.applyBC(U010)
#        #m.applyBC(U011)
#        m.applyBC(U100)
#        #m.applyBC(U101)
#        m.applyBC(U110)
#        m.applyBC(U111)
#        
#        import pylab as plt; import numpy as np
#        X = m.X; getSol = m.param.initFunction.GetExactSolution; Udt = getSol(m,dt); Udemi = getSol(m,0.5*dt)
#        UL1Dt = np.mean(m.UBar(Udt),axis=0);UL1Demi = np.mean(m.UBar(Udemi),axis=0);
#        #Err0 = m.UBar(np.abs(U0-Udt))/UBarDt; Err1 = m.UBar(np.abs(U1-Udt))/UBarDt;Err00 = m.UBar(np.abs(U00-Udemi))/UBarDemi;Err10 = m.UBar(np.abs(U10-Udemi))/UBarDemi;Err11 = m.UBar(np.abs(U11-Udemi))/UBarDemi; Err000 = m.UBar(np.abs(U000-Udt))/UBarDt;Err100 = m.UBar(np.abs(U100-Udt))/UBarDt;Err110 = m.UBar(np.abs(U110-Udt))/UBarDt; Err111 = m.UBar(np.abs(U111-Udt))/UBarDt;
#        Err0 = m.UBar(np.abs(U0-Udt))/UL1Dt; Err1 = m.UBar(np.abs(U1-Udt))/UL1Dt;Err00 = m.UBar(np.abs(U00-Udemi))/UL1Demi;Err10 = m.UBar(np.abs(U10-Udemi))/UL1Demi;Err11 = m.UBar(np.abs(U11-Udemi))/UL1Demi; Err000 = m.UBar(np.abs(U000-Udt))/UL1Dt;Err100 = m.UBar(np.abs(U100-Udt))/UL1Dt;Err110 = m.UBar(np.abs(U110-Udt))/UL1Dt; Err111 = m.UBar(np.abs(U111-Udt))/UL1Dt;
#        nMom = np.shape(Err0)[-1];legende = (); 
#        for i in np.arange(nMom): legende += ('Err_M'+str(i)+'/||M'+str(i)+'||_1',)
#        #plt.plot(X,Err0);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err0.png');plt.show();plt.plot(X,Err1);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err1.png');plt.show();plt.plot(X,Err00);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err00.png');plt.show();plt.plot(X,Err10);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err10.png');plt.show();plt.plot(X,Err11);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err11.png');plt.show();plt.plot(X,Err000);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err000.png');plt.show();plt.plot(X,Err100);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err100.png');plt.show();plt.plot(X,Err110);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err110.png');plt.show();plt.plot(X,Err111);plt.legend(legende,loc='best');plt.savefig(str(nMom)+'Moments_Err111.png');plt.show();
#                
#        return U111
    def name(self): 
        return "RK3SSP"
        
""" ******************** Runge-Kutta 4 *********************"""

class RK4(timeStep):
    def step(self,U,dt,scheme,eq,m):
        ### Step_1
        RHS_1 = scheme.RHS(U,eq,m)
        U_1   = U + 0.5*dt*RHS_1
        U_1   = scheme.positivityLimitation(U_1,eq,m)
        m.applyBC(U_1)
        ### Step_2
        RHS_2 = scheme.RHS(U_1,eq,m)
        U_2   = U + 0.5*dt*RHS_2
        U_2   = scheme.positivityLimitation(U_2,eq,m)
        m.applyBC(U_2)
        ### Step_3
        RHS_3 = scheme.RHS(U_2,eq,m)
        U_3   = U + dt*RHS_3
        U_3   = scheme.positivityLimitation(U_3,eq,m)
        m.applyBC(U_3)
        ### Step_1
        RHS_4 = scheme.RHS(U_3,eq,m)
        U     = U + dt/6.0*(RHS_1+2.0*RHS_2+2.0*RHS_3+RHS_4)
        U     = scheme.positivityLimitation(U,eq,m)
        m.applyBC(U)
        return U
    def name(self): 
        return "RK4"
        